var selected_path_circle = [],
	p_element = [],

	$table_holder,
	max_select_el = 4,
	text,
	first_circle,
	first = false,
	$table_hline;


var ob_values1 = [
	{
		"selector": false,
		"id": 0,
		"bgcolor": '#9B1944',
		"percentage": 43,
		"name": "",
	},
	{
		"selector": false,
		"id": 1,
		"bgcolor": '#00AEEF',
		"percentage": 57,
		"name": ""
	},
];

var ob_values2 = [
	{
		"selector": false,
		"id": 0,
		"bgcolor": '#9B1944',
		"percentage": 53,
		"name": "",
	},
	{
		"selector": false,
		"id": 1,
		"bgcolor": '#00AEEF',
		"percentage": 47,
		"name": ""
	},
];

var ob_values3 = [
	{
		"selector": '#table_holder',
		"id": 0,
		"bgcolor": '#00aeef',
		"percentage": 29,
		"name": "West and Central Africa",
	},
	{
		"selector": '#table_holder',
		"id": 1,
		"bgcolor": '#82cff4',
		"percentage": 28,
		"name": "East and Southern Africa"
	},
	{
		"selector": '#table_holder',
		"id": 2,
		"bgcolor": '#a1d9f7',
		"percentage": 16,
		"name": "Middle East & North Africa"
	},
	{
		"selector": '#table_holder',
		"id": 3,
		"bgcolor": '#3b4394',
		"percentage": 13,
		"name": "South Asia"
	},
	{
		"selector": '#table_holder',
		"id": 4,
		"bgcolor": '#9a99cb',
		"percentage": 7,
		"name": "East Asia and Paciic"
	},
	{
		"selector": '#table_holder',
		"id": 5,
		"bgcolor": '#7f7a7c',
		"percentage": 4,
		"name": "Europe and Central Asia"
	},
	{
		"selector": '#table_holder',
		"id": 6,
		"bgcolor": '#bdb8b9',
		"percentage": 3,
		"name": "Latin America and the Caribbean"
	}
];

var ob_values4 = [
	{
		"selector": false,
		"tooltipParent": '#tooltip_parent',
		"mod": 'pos_1',
		"id": 0,
		"bgcolor": '#ea610a',
		"percentage": 31.8,
		"subName": "Services ",
		"name": "$1.108 billion"
	},
	{
		"selector": false,
		"tooltipParent": '#tooltip_parent',
		"mod": 'pos_2',
		"id": 1,
		"bgcolor": '#00aeef',
		"percentage": 68.2,
		"subName": "Goods",
		"name": "$2.378 billion",
		"totalTextParent": '#graph_total',
		"totalText": "$3.486 billion"
	}
];

var ob_values5 = [
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 0,
		"bgcolor": '#00aeef',
		"percentage": 0,
		"name": "HOVER TO SEE DATA"
	},
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 1,
		"bgcolor": '#00aeef',
		"percentage": 51,
		"name": "Sub-Saharan Africa"
	},
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 2,
		"bgcolor": '#0cbaee',
		"percentage": 29,
		"name": "Asia"
	},
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 3,
		"bgcolor": '#a1d9f7',
		"percentage": 15,
		"name": "Middle East & North Africa"
	},
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 4,
		"bgcolor": '#ea610a',
		"percentage": 4,
		"name": "Central & Eastern Europe"
	},
	{
		"selector": false,
		"graphInfo": '#graph_info',
		"id": 5,
		"bgcolor": '#f4a56d',
		"percentage": 1,
		"name": "Central & South American and the Caribbean"
	}
];

var ob_values6 = [
	{
		"selector_2": "#graph_list",
		"id": 0,
		"bgcolor": '#00aeef',
		"percentage": 95,
		"name": "Copenhagen $105.1 million"
	},
	{
		"selector_2": "#graph_list",
		"id": 1,
		"bgcolor": '#82cff4',
		"percentage": 4,
		"name": "Dubai $4.9 million "
	},
	{
		"selector_2": "#graph_list",
		"id": 2,
		"bgcolor": '#1d1d1b',
		"percentage": 1,
		"name": "Panama $92,500"
	}
];

var ob_values7 = [
	{
		"selector": false,
		"id": 0,
		"bgcolor": '#00aeef',
		"percentage": 71,
		"name": ""
	},
	{
		"selector": false,
		"single_info": "#graph_info_title_w",
		"id": 1,
		"bgcolor": '#ea610a',
		"percentage": 29,
		"name": ""
	}
];

// var ob_values4 = [
// 	{
// 		"selector": false,
// 		"id": 0,
// 		"bgcolor": '#00aeef',
// 		"percentage": 29,
// 		"name": "West and Central Africa"
// 	},
// 	{
// 		"selector": false,
// 		"id": 1,
// 		"bgcolor": '#82cff4',
// 		"percentage": 28,
// 		"name": "East and Southern Africa "
// 	},
// 	{
// 		"selector": false,
// 		"id": 2,
// 		"bgcolor": '#bce3f9',
// 		"percentage": 16,
// 		"name": "Middle East and North Africa"
// 	},
// 	{
// 		"selector": false,
// 		"id": 3,
// 		"bgcolor": '#3b4394',
// 		"percentage": 13,
// 		"name": "South Asia",
// 	},
// 	{
// 		"selector": false,
// 		"id": 4,
// 		"bgcolor": '#3b4394',
// 		"percentage": 7,
// 		"name": "East Asia and Paciic",
// 	},
// 	{
// 		"selector": false,
// 		"id": 5,
// 		"bgcolor": '#7f7a7c',
// 		"percentage": 4,
// 		"name": "Europe and Central Asia",
// 	},
// 	{
// 		"selector": false,
// 		"id": 6,
// 		"bgcolor": '#7f7a7c',
// 		"percentage": 3,
// 		"name": "Latin America and the Caribbean",
// 	}
// ];

Raphael.fn.pieChart = function (cx, cy, r, sub_r, values, stroke, version) {
	var paper = this,
		rad = Math.PI / 180,
		chart = this.set(),
		segments_array = [],
		segments_text_array_2 = [],
		segments_text_array = [];

	var start_pos;

	//функция для создания сектора
	function sector(cx, cy, r, startAngle, endAngle, params) {
		var x1 = cx + r * Math.cos(-startAngle * rad),
			x2 = cx + r * Math.cos(-endAngle * rad),
			y1 = cy + r * Math.sin(-startAngle * rad),
			y2 = cy + r * Math.sin(-endAngle * rad);
		return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
	}

	var angle = 90,
		total = 0,
		start = 0,
		max_el = values.length,
		delta_r = sub_r + 5,
		compare_circle_r = 25;

	var process = function (j) {
		var small_el,
			delta = 0,
			txt,
			image,
			circle,
			x_leble,
			y_leble,
			value = values[j].percentage;

		// console.log(value)

		var angleplus = 360 * value / total;
		if (!first_circle) {
			angle = angle;
			first_circle = true;
		}

		if (angleplus < 30) {
			delta = 0;
			small_el = true;
		}

		var popangle = angle + angleplus / 2,
			ms = 100,
			bcolor = values[j].bgcolor;

		var p = sector(
			cx,
			cy,
			r,
			angle,
			angle + angleplus,
			{fill: bcolor, stroke: stroke, "stroke-width": 0});

		var r_coef = .75;

		if (version === 0) {
			r_coef = 1.2;
		}

		if (version === 1 && values[j].percentage < 10) {
			r_coef = 1.08;
		}

		//1.2 отвечает за вынос текста за кружок
		x_leble = cx + (r * r_coef) * Math.cos(-popangle * rad);
		y_leble = cy + (r * r_coef) * Math.sin(-popangle * rad);
		// var x_leble = cx;
		// var y_leble = cy;
		//создание текстового блока
		if (version !== 4) {
			var text;
			if (version === 1) {
				text = paper.text(
					x_leble,
					values[j].percentage < 10 ? y_leble - (values[j].percentage * values[j].percentage - values[j].percentage * values[j].percentage * .11) : y_leble,
					values[j].percentage + '%')
					.attr({
						"fill": values[j].percentage < 10 ? "#000" : "#fff",
						"font-size": 18,
						"font-weight": 700,
						"font-family": "Roboto, PT Sans, Helvetica, sans-serif",
						"text-align": "center"
					})
				if(values[j].percentage < 10){
					paper.path(["M", x_leble, y_leble, "V", y_leble - (values[j].percentage * values[j].percentage - values[j].percentage * values[j].percentage * .11) ]).attr({stroke: "#000", "stroke-dasharray":". "})
					TweenLite.set(text, {raphael: {tx:14, transformOrigin: "50% 50%", scaleX: -1}});
				}else {
					TweenLite.set(text, {raphael: {transformOrigin: "50% 50%", scaleX: -1}});
				}

			} else {
				text = paper.text(
					x_leble,
					y_leble,
					version !== 2 && version !== 5 ? values[j].percentage + '%' : '')
					.attr({
						"fill": version === 0 ? bcolor : "#fff",
						"font-size": 18,
						"font-weight": 700,
						"font-family": "Roboto, PT Sans, Helvetica, sans-serif",
						"text-align": "center"
					})
				TweenLite.set(text, {raphael: {transformOrigin: "50% 50%", scaleX: -1}});
			}

		}

		var x_sub_leble = cx + (r - delta_r) * Math.cos(-popangle * rad);
		var y_sub_leble = cy + (r - delta_r) * Math.sin(-popangle * rad);
		var x_small_circle = cx + (r - delta_r - 3) * Math.cos(-popangle * rad);
		var y_small_circle = cy + (r - delta_r - 3) * Math.sin(-popangle * rad);
		var x_compare_circle = cx + (r - delta_r - compare_circle_r - 10) * Math.cos(-popangle * rad);
		var y_compare_circle = cy + (r - delta_r - compare_circle_r - 10) * Math.sin(-popangle * rad);
		if (version === 1 || version === 5) {
			append_el(
				{
					selector: values[j].selector,
					selector_2: values[j].selector_2,
					id: values[j].id,
					bgcolor: values[j].bgcolor,
					name: values[j].name,
					percentage: values[j].percentage,
					single_info: values[j].single_info
				}
			);
		}

		if (version === 4) {
			append_el(
				{
					selector: values[j].selector,
					id: values[j].id,
					bgcolor: values[j].bgcolor,
					name: values[j].name,
					percentage: values[j].percentage,
					graphInfo: values[j].graphInfo
				}
			);
		}

		if (version === 2) {
			append_el(
				{
					selector: values[j].selector,
					id: values[j].id,
					bgcolor: values[j].bgcolor,
					name: values[j].name,
					percentage: values[j].percentage,
					tooltipParent: values[j].tooltipParent,
					subName: values[j].subName,
					mod: values[j].mod,
					totalTextParent: values[j].totalTextParent,
					totalText: values[j].totalText
				}
			);
		}

		// if (version === 4) {
		// 	append_el(values[j].selector, values[j].id, values[j].bgcolor, values[j].name, values[j].percentage, false, values[j].tooltipParent, values[j].subName)
		// }
		values[j].center_x = x_sub_leble;
		values[j].center_y = y_sub_leble;
		values[j].center_circle_x = x_small_circle;
		values[j].center_circle_y = y_small_circle;
		values[j].center_compare_circle_x = x_compare_circle;
		values[j].center_compare_circle_y = y_compare_circle;


		if (version === 3) {
			p.mouseover(function () {
				$('.id_' + values[j].id).addClass('active');
				TweenLite.to(p, .5, {raphael: {opacity: .8}, ease: Power1.easeInOut});
				// text.toFront();
				TweenLite.to(text, .5, {raphael: {opacity: 1, transformOrigin: "50% 50%", scaleX: -1}, ease: Power1.easeInOut});
				// p.stop().animate({opacity: 0.8}, ms, "elastic");
			}).mouseout(function () {
				$('.id_' + values[j].id).removeClass('active');
				// p.stop().animate({opacity: 1}, ms);
				TweenLite.to(p, .5, {raphael: {opacity: 1}, ease: Power1.easeInOut});
				TweenLite.to(text, .5, {raphael: {opacity: 0, transformOrigin: "50% 50%", scaleX: -1}, ease: Power1.easeInOut});
			});
		}

		if (version === 4 && j > 0) {
			// console.log('segments_array', segments_array[values[j].id]);
			p.mouseover(function () {
				// console.log('segments_array', segments_array[values[j].id])
				TweenLite.to(segments_array[values[j - 1].id], .3, {raphael: {opacity: 1}, ease: Power1.easeInOut});
				TweenLite.to(segments_text_array[values[j - 1].id], .3, {opacity: 1, ease: Power1.easeInOut});
			}).mouseout(function () {
				TweenLite.to(segments_array[values[j - 1].id], .3, {raphael: {opacity: 0}, ease: Power1.easeInOut})
				TweenLite.to(segments_text_array[values[j - 1].id], .3, {opacity: 0, ease: Power1.easeInOut})
			});
		}

		if (version === 2) {

			p.mouseover(function () {
				TweenLite.to(segments_text_array_2[values[j].id], .3, {opacity: 1, ease: Power1.easeInOut});
			}).mouseout(function () {
				TweenLite.to(segments_text_array_2[values[j].id], .3, {opacity: 0, ease: Power1.easeInOut})
			});

		}

		var valueToPush = {
			'element': p,
			'element_id': values[j].id
		}

		p_element.push(valueToPush);

		angle += angleplus;
		chart.push(p);
		start += .1;
	};


	var process_text = function (j) {
		var small_el,
			delta = 0,
			txt,
			image,
			circle,
			x_leble,
			y_leble,
			value = values[j].percentage;

		var bcolor = values[j].bgcolor;


		//создание текстового блока
		// var text = paper.text(
		// 		cx,
		// 		cy,
		// 		values[j].percentage + '%\n'+values[j].name)
		// 		.attr({
		// 			"fill":"#fff",
		// 			"font-size": 18,
		// 			"font-weight": 700,
		// 			"font-family": "Roboto",
		// 			"text-align": "center"
		// 		});

		if (version === 4) {
			var color_circle = paper.circle(cx, cy, sub_r + 1).attr({fill: bcolor, stroke: stroke, "stroke-width": 0});
			// if (j == 0) {
			// 	TweenLite.set(color_circle, {raphael: {opacity: 0}});
			// 	TweenLite.set($('.graph_info_block')[j], {opacity: 0});
			//
			// 	// console.log();
			// 	segments_array.push(color_circle);
			// 	segments_text_array.push($('.graph_info_block')[j]);
			// }
			if (j > 0) {
				TweenLite.set(color_circle, {raphael: {opacity: 0}});
				TweenLite.set($('.graph_info_block')[j], {opacity: 0});

				// console.log();
				segments_array.push(color_circle);
				segments_text_array.push($('.graph_info_block')[j]);
			}

		}

		if (version === 2) {
			TweenLite.set($('.graph_tooltip_item')[j], {opacity: 0});

			segments_text_array_2.push($('.graph_tooltip_item')[j]);
		}

	};

	//суммирование всех процентов на случай если оператор идиот и не умеет считать
	for (var i = 0, ii = max_el; i < ii; i++) {
		total += values[i].percentage;
	}
	// создание каждого сегмента
	for (i = 0; i < ii; i++) {
		process(i);

	}
	// заполнение центра
	paper.circle(cx, cy, r - sub_r).attr({fill: "#fff", stroke: stroke, "stroke-width": 0});

	if (version === 4 || version === 2) {
		// для случая когда есть текст внутри белого кружка
		for (i = 0; i < ii; i++) {
			process_text(i);
		}

	}


	return chart;
};

function append_el(options) {
	// блок с описанием каждого сегмента
	if (options.link) {
		$(selector).append('<li class="table_holder_item id_' + options.id + '">' +
			'<div class="table_h_i_color" style="background: ' + options.bgcolor + '"  data-el-id="' + options.id + '"></div>' +
			'<div class="table_h_i_title"  data-el-id="' + options.id + '">' + options.name + '</div>' +
			'<a class="table_h_i_butt" href="http://www.itcentralstation.com/products/comparisons/' + options.link + '" title="Compare Side-by-Side" style="background: ' + options.bgcolor + '">Compare Side-by-Side</a>' +
			'</li>');
	}
	else if (options.tooltipParent) {
		$(options.totalTextParent).text(options.totalText);
		$(options.tooltipParent).append('<li class="graph_tooltip_item ' + options.mod + '" style="background: ' + options.bgcolor + '">' +
			'<span class="graph_tooltip_subtitle">' + options.subName + '</span>' +
			'<span class="graph_tooltip_title">' + options.name + '</span>' +
			'</li>');
	}
	else if (options.graphInfo) {
		if ( options.percentage == 0 ) {
			$(options.graphInfo).append('<div class="graph_info_block" style="background: #FFFFFF">' +
				'<span class="graph_info_block_name" style="color:#00aeef;">' + options.name + '</span>' +
				'</div>');
		}else{
			$(options.graphInfo).append('<div class="graph_info_block" style="background: ' + options.bgcolor + '">' +
				'<span class="graph_info_block_percent">' + options.percentage + '%</span>' +
				'<span class="graph_info_block_name">' + options.name + '</span>' +
				'</div>');
		}

	}
	else if (options.selector_2) {
		$(options.selector_2).append('<li class="table_holder_item center_mod id_' + options.id + '">' +
			'<span class="table_h_i_title" data-el-id="' + options.id + '" style="color: ' + options.bgcolor + '">' + options.name + '</span>' +
			'</li>');
	}
	else if (options.single_info) {
		$(options.single_info).append('<span class="tooltip_graph_in sm_mod" style="color: ' + options.bgcolor + '">' + options.percentage + '%</span>');
	}
	else {
		$(options.selector).append('<li class="table_holder_item id_' + options.id + '">' +
			'<div class="table_h_i_color" style="background: ' + options.bgcolor + '" data-el-id="' + options.id + '"></div>' +
			'<div class="table_h_i_title" data-el-id="' + options.id + '">' + options.name + '</div>' +
			'</li>');
	}

}

$(function () {
	ob_values3.sort(function (a, b) {
		return b.percentage - a.percentage;
	});

	var total = 0;

	var total = ob_values3.reduce(function (a, b) {
		return {percentage: a.percentage + b.percentage}; // returns object with property x
	})

	function logArrayElements(element, index, array) {
		element.percentage = Math.round10(element.percentage / (total.percentage / 100), -2);
	}

	// Обратите внимание на пропуск по индексу 2, там нет элемента, поэтому он не посещается
	ob_values3.forEach(logArrayElements);


	$table_holder = $('#table_holder');
	$table_hline = $('#table_hline');

	$table_holder.on('click', '.table_h_i_color, .table_h_i_title', function () {
		var $id = parseInt($(this).attr('data-el-id'));
		for (var i = 0; i < p_element.length; i++) {
			if ($id === p_element[i].element_id && p_element[i].element.events !== undefined) {
				console.log(p_element[i].element)
				p_element[i].element.events[2].f();
			}
		}
	});

	$('#page_views').text(' 72,193 ');

	$('#comparisons').text(' 49,532 ');

	var test_array = {};

	// console.log(total)


});

$(window).on('load', loadFunc2);

function loadFunc2() {
	// $('#holder4').addClass('loaded');
	//описание по каждому элементу
	// айди блока, х, y, центр x, центр y, радиус, толщина цветной плоскости, массив с данными, цвет линии обводки, вид графика

	if ($('#holder1').length) {
		$('#holder1').addClass('loaded');
		Raphael("holder1", 342, 182).pieChart(171, 91, 90, 50, ob_values1, "#fff", 0);
	}

	if ($('#holder2').length) {
		$('#holder2').addClass('loaded');
		Raphael("holder2", 342, 182).pieChart(171, 91, 90, 50, ob_values2, "#fff", 0);
	}

	if ($('#holder3').length) {
		$('#holder3').addClass('loaded');
		var holder3 = Raphael("holder3", 350, 400);
				holder3.setViewBox(0,0,350,400,true);
				holder3.setSize('100%', '100%');
				holder3.pieChart(175, 200, 162, 70, ob_values3, "#fff", 1);
	}

	if ($('#holder4').length) {
		$('#holder4').addClass('loaded');
		Raphael("holder4", 268, 268).pieChart(134, 134, 132, 70, ob_values4, "#fff", 2);
	}

	if ($('#holder5').length) {
		$('#holder5').addClass('loaded');
		Raphael("holder5", 280, 280).pieChart(140, 140, 140, 70, ob_values5, "#fff", 4);
	}

	if ($('#holder6').length) {
		$('#holder6').addClass('loaded');
		Raphael("holder6", 150, 150).pieChart(75, 75, 75, 40, ob_values6, "#fff", 5);
	}

	if ($('#holder7').length) {
		$('#holder7').addClass('loaded');
		Raphael("holder7", 150, 150).pieChart(75, 75, 75, 40, ob_values7, "#fff", 5);
	}


	// TweenMax.staggerFrom($('#holder path'), .6, {opacity:0, delay:.5},.05)
	// TweenMax.staggerFrom($('.table_holder_item'), .6, {opacity:0, delay:.5},.05, end)
	// function end() {
	// 	$( "#holder text" ).each(function( index ) {
	// 			  var $this = $(this);
	// 		$('#holder svg').append($this);
	// 			});
	// }

}

function test() {
	var r = Raphael("holder");

	r.customAttributes.segment = function (x, y, r, a1, a2) {
		var flag = (a2 - a1) > 180,
				clr = (a2 - a1) / 360;
		a1 = (a1 % 360) * Math.PI / 180;
		a2 = (a2 % 360) * Math.PI / 180;
		return {
			path: [["M", x, y], ["l", r * Math.cos(a1), r * Math.sin(a1)], ["A", r, r, 0, +flag, 1, x + r * Math.cos(a2), y + r * Math.sin(a2)], ["z"]],
			fill: "hsb(" + clr + ", .75, .8)"
		};
	};

	function animate(ms) {
		var start = 0,
				val;
		for (i = 0; i < ii; i++) {
			val = 360 / total * data[i];
			paths[i].animate({segment: [200, 200, 150, start, start += val]}, ms || 1500, "bounce");
			paths[i].angle = start - val / 2;
		}
	}

	var data = [24, 92, 24, 52, 78, 99, 82, 27],
			paths = r.set(),
			total,
			start,
			bg = r.circle(200, 200, 0).attr({stroke: "#fff", "stroke-width": 0});
	data = data.sort(function (a, b) { return b - a;});

	total = 0;
	for (var i = 0, ii = data.length; i < ii; i++) {
		total += data[i];
	}
	start = 0;
	for (i = 0; i < ii; i++) {
		var val = 360 / total * data[i];
		(function (i, val) {
			paths.push(r.path().attr({segment: [200, 200, 1, start, start + val], stroke: "#fff"}).click(function () {
				total += data[i];
				data[i] *= 2;
				animate();
			}));
		})(i, val);
		start += val;
	}
	bg.animate({r: 151}, 1000, "bounce");
	animate(1000);
	var t = r.text(200, 20, "Click on segments to make them bigger.").attr({
		font: '100 20px "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial, sans-serif',
		fill: "#fff"
	});
}

(function () {
	/**
	 * Корректировка округления десятичных дробей.
	 *
	 * @param {String}  type  Тип корректировки.
	 * @param {Number}  value Число.
	 * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
	 * @returns {Number} Скорректированное значение.
	 */
	function decimalAdjust(type, value, exp) {
		// Если степень не определена, либо равна нулю...
		if (typeof exp === 'undefined' || +exp === 0) {
			return Math[type](value);
		}
		value = +value;
		exp = +exp;
		// Если значение не является числом, либо степень не является целым числом...
		if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
			return NaN;
		}
		// Сдвиг разрядов
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		// Обратный сдвиг
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	}

	// Десятичное округление к ближайшему
	if (!Math.round10) {
		Math.round10 = function (value, exp) {
			return decimalAdjust('round', value, exp);
		};
	}
	// Десятичное округление вниз
	if (!Math.floor10) {
		Math.floor10 = function (value, exp) {
			return decimalAdjust('floor', value, exp);
		};
	}
	// Десятичное округление вверх
	if (!Math.ceil10) {
		Math.ceil10 = function (value, exp) {
			return decimalAdjust('ceil', value, exp);
		};
	}
})();
