
function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

am4core.ready(function() {
  am4core.useTheme(am4themes_animated);
  if ($('#am1').length) {

    // Create chart instance
    var chart = am4core.create("am1", am4charts.PieChart);

    chart.data = [{
      "country": "West and Central Africa",
      "litres": 29
    }, {
      "country": "East and Southern Africa",
      "litres": 28
    }, {
      "country": "Middle East & North Africa",
      "litres": 16
    }, {
      "country": "South Asia",
      "litres": 13
    }, {
      "country": "East Asia and Pacific",
      "litres": 7
    }, {
      "country": "Europe and Central Asia",
      "litres": 4
    }, {
      "country": "Latin America and the Caribbean",
      "litres": 3
    }];
    chart.innerRadius = am4core.percent(50);

    // Add and configure Series
    // var pieSeries = chart.series.push(new am4charts.PieSeries());
    // pieSeries.dataFields.value = "litres";
    // pieSeries.dataFields.category = "country";
    // pieSeries.innerRadius = am4core.percent(50);
    // pieSeries.ticks.template.disabled = true;
    // pieSeries.labels.template.disabled = true;

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.colors.list = [
        new am4core.color('#00aeef'),
        new am4core.color('#82cff4'),
        new am4core.color('#a1d9f7'),
        new am4core.color('#3b4394'),
        new am4core.color('#9a99cb'),
        new am4core.color('#7f7a7c'),
        new am4core.color('#bdb8b9'),
    ];
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";

    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.labels.template.disabled = true;
    pieSeries.tooltip.label.minWidth = 60;
    pieSeries.tooltip.label.minHeight = 100;
    pieSeries.tooltip.label.textAlign = "middle";
    pieSeries.tooltip.label.textValign = "middle";
    //pieSeries.tooltip.background.filters.clear();
    pieSeries.tooltip.fontSize = 16;
    pieSeries.tooltip.background.strokeOpacity = 0;

    pieSeries.tooltip.autoTextColor = false;
    pieSeries.tooltip.label.fill = am4core.color("#FFFFFF");
    pieSeries.slices.template.tooltipText = "{category}: {value.value}%";

    let shadow = pieSeries.tooltip.background.filters.getIndex(0);
    shadow.dx = 0;
    shadow.dy = 15;
    shadow.blur = 10;
    shadow.opacity = 0.1;
    shadow.color = am4core.color("#000000");

    chart.legend = new am4charts.Legend();
    chart.legend.position = "right";
    chart.legend.fontSize = 16;
    //console.log(chart.legend);
    chart.legend.valueLabels.template.disabled = true;
    chart.legend.itemContainers.template.togglable = false;
    chart.legend.itemContainers.template.events.on("hit", function(ev) {
      var slice = ev.target.dataItem.dataContext.slice;
      slice.isActive = !slice.isActive;
    });

    chart.legend.useDefaultMarker = true;
    var marker = chart.legend.markers.template.children.getIndex(0);
    marker.cornerRadius(12, 12, 12, 12);
  }

  if ($('#am2').length) {


    // Create chart instance
    var chart2 = am4core.create("am2", am4charts.XYChart);
    chart2.colors.list = [
      am4core.color("#00aeef"),
      am4core.color("#ea610a"),
    ];
    // Add data
    chart2.data = [
      {
    		year: '2018',
    		target: 260,
    		above: 91.2
    	},
    	{
    		year: '2019',
    		target: 290
    	},
    	{
    		year: '2020',
    		target: 70
    	},
    	{
    		year: '2021',
    		target: 70
    	}
    ];

    // Create axes
    var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.grid.template.disabled = true;

    var  valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.title.text = "millions";
    valueAxis.renderer.grid.template.disabled = true;
    // Create series
    function createSeries(field, name, stacked,color) {
      var series = chart2.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = field;
      series.dataFields.categoryX = "year";
      series.name = name;
      series.color = color;
      series.columns.template.tooltipText = "{name}: ${valueY} million";
      series.stacked = stacked;
      series.columns.template.width = am4core.percent(95);

      series.tooltip.label.minWidth = 60;
      series.tooltip.label.minHeight = 100;
      series.tooltip.label.textAlign = "middle";
      series.tooltip.label.textValign = "middle";
      //series.tooltip.background.filters.clear();
      series.tooltip.fontSize = 16;
      series.tooltip.background.strokeOpacity = 0;

      series.tooltip.autoTextColor = false;
      series.tooltip.label.fill = am4core.color("#FFFFFF");

      let shadow = series.tooltip.background.filters.getIndex(0);
      shadow.dx = 0;
      shadow.dy = 15;
      shadow.blur = 10;
      shadow.opacity = 0.1;
      shadow.color = am4core.color("#000000");

    }


    createSeries("target", "Target", false,'#000000');
    createSeries("above", "Savings above the target", true,'#000000');



    // Add legend
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = "right";
    chart2.legend.fontSize = 16;

    chart2.legend.itemContainers.template.clickable = false;
    chart2.legend.itemContainers.template.focusable = false;
    chart2.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;

    chart2.legend.useDefaultMarker = true;
    var marker = chart2.legend.markers.template.children.getIndex(0);
    marker.cornerRadius(12, 12, 12, 12);
    marker.width = 15;
    marker.height = 15;
  }

  if ( $('#gf7').length ) {

    // Create chart instance
    var chart3 = am4core.create("gf7", am4charts.PieChart);

    chart3.data = [{
      "country": "Copenhagen",
      "litres": 95,
      "mil": "$105.1 million"
    }, {
      "country": "Dubai",
      "litres": 4,
      "mil": "$4.9 million"
    }, {
      "country": "Panama",
      "litres": 1,
      "mil": "$92,500"
    }
    ];
    chart3.innerRadius = am4core.percent(50);

    // Add and configure Series
    // var pieSeries = chart.series.push(new am4charts.PieSeries());
    // pieSeries.dataFields.value = "litres";
    // pieSeries.dataFields.category = "country";
    // pieSeries.innerRadius = am4core.percent(50);
    // pieSeries.ticks.template.disabled = true;
    // pieSeries.labels.template.disabled = true;

    var pieSeries = chart3.series.push(new am4charts.PieSeries());
    pieSeries.colors.list = [
        new am4core.color('#00aeef'),
        new am4core.color('#82cff4'),
        new am4core.color('#1d1d1b'),
    ];
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";

    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.labels.template.disabled = true;

    pieSeries.tooltip.label.minWidth = 60;
    pieSeries.tooltip.label.minHeight = 100;
    pieSeries.tooltip.label.textAlign = "middle";
    pieSeries.tooltip.label.textValign = "middle";
    //pieSeries.tooltip.background.filters.clear();
    pieSeries.tooltip.fontSize = 16;
    pieSeries.tooltip.background.strokeOpacity = 0;

    pieSeries.tooltip.autoTextColor = false;
    pieSeries.tooltip.label.fill = am4core.color("#FFFFFF");
    pieSeries.slices.template.tooltipText = "{category}: {value.value}%";

    let shadow = pieSeries.tooltip.background.filters.getIndex(0);
    shadow.dx = 0;
    shadow.dy = 15;
    shadow.blur = 10;
    shadow.opacity = 0.1;
    shadow.color = am4core.color("#000000");

    // chart3.legend = new am4charts.Legend();
    // chart3.legend.position = "right";
    // chart3.legend.fontSize = 16;
    // //console.log(chart.legend);
    // chart3.legend.valueLabels.template.disabled = true;
    // chart3.legend.itemContainers.template.togglable = false;
    // chart3.legend.itemContainers.template.events.on("hit", function(ev) {
    //   var slice = ev.target.dataItem.dataContext.slice;
    //   slice.isActive = !slice.isActive;
    // });
    //
    // chart3.legend.useDefaultMarker = true;
    // var marker = chart3.legend.markers.template.children.getIndex(0);
    // marker.cornerRadius(12, 12, 12, 12);

  }

  if ( $('#gf8').length ) {

    // Create chart instance
    var chart4 = am4core.create("gf8", am4charts.PieChart);

    chart4.data = [{
      "country": "Regular programmes",
      "litres": 71
    }, {
      "country": "Emergency response",
      "litres": 29
    }
    ];
    chart4.innerRadius = am4core.percent(50);

    // Add and configure Series
    // var pieSeries = chart.series.push(new am4charts.PieSeries());
    // pieSeries.dataFields.value = "litres";
    // pieSeries.dataFields.category = "country";
    // pieSeries.innerRadius = am4core.percent(50);
    // pieSeries.ticks.template.disabled = true;
    // pieSeries.labels.template.disabled = true;

    var pieSeries = chart4.series.push(new am4charts.PieSeries());
    pieSeries.colors.list = [
        new am4core.color('#00aeef'),
        new am4core.color('#ea610a'),
    ];
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";

    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.labels.template.disabled = true;
    pieSeries.tooltip.label.minWidth = 60;
    pieSeries.tooltip.label.minHeight = 100;
    pieSeries.tooltip.label.textAlign = "middle";
    pieSeries.tooltip.label.textValign = "middle";
    //pieSeries.tooltip.background.filters.clear();
    pieSeries.tooltip.fontSize = 16;
    pieSeries.tooltip.background.strokeOpacity = 0;

    pieSeries.tooltip.autoTextColor = false;
    pieSeries.tooltip.label.fill = am4core.color("#FFFFFF");
    pieSeries.slices.template.tooltipText = "{category}: {value.value}%";

    let shadow = pieSeries.tooltip.background.filters.getIndex(0);
    shadow.dx = 0;
    shadow.dy = 15;
    shadow.blur = 10;
    shadow.opacity = 0.1;
    shadow.color = am4core.color("#000000");

    // chart4.legend = new am4charts.Legend();
    // chart4.legend.position = "right";
    // chart4.legend.fontSize = 16;
    // //console.log(chart.legend);
    // chart4.legend.valueLabels.template.disabled = true;
    // chart4.legend.itemContainers.template.togglable = false;
    // chart4.legend.itemContainers.template.events.on("hit", function(ev) {
    //   var slice = ev.target.dataItem.dataContext.slice;
    //   slice.isActive = !slice.isActive;
    // });
    //
    // chart4.legend.useDefaultMarker = true;
    // var marker = chart4.legend.markers.template.children.getIndex(0);
    // marker.cornerRadius(12, 12, 12, 12);

  }

}); // end am4core.ready()
;// variables:
// x - horizontal aligment, year
// y - vertical aligment, min - 0, max - 0
// r - radius, percentage of height


let bubles = new Map(
  [
    [
      'healthDevices', {
        title: 'Health devices',
        color: '#00aeef',
        arr: [
          {
            hint: 'Scaling pneumonia response innovations',
            x: 2019,
            y: 69.6,
            r: 23.2,
          },
          {
            hint: 'Uterine balloon tamponade',
            x: 2019.8,
            y: 79.6,
            r: 17,
          },
          {
            hint: 'Acute respiratory infection diagnostic aid',
            x: 2020.2,
            y: 40,
            r: 17,
          },
          {
            hint: 'Zika diagnostics',
            x: 2021.4,
            y: 60,
            r: 17,
          },
          {
            hint: 'Neonatal hypothermia detection device',
            x: 2021,
            y: 70,
            r: 12,
          },
          {
            hint: 'Non-pneumatic anti-shock garment',
            x: 2021.7,
            y: 45,
            r: 22,
          },
          {
            hint: 'Neonatal resuscitators',
            x: 2021.5,
            y: 20,
            r: 12,
          },
          {
            hint: 'Drone delivery solutions for hard to reach populations',
            x: 2029,
            y: 70,
            r: 12,
          },
          {
            hint: 'Differential diagnostic tools for fever',
            x: 2027,
            y: 50,
            r: 19,
          }
        ]
      }
    ],
    [
      'Immunization', {
        title: 'Immunization',
        color: '#007c39;',
        arr: [
          {
            hint: 'Affordable pneumococcal conjugate vaccine  accessible for middle-income countries',
            x: 2020,
            y: 60,
            r: 20,
          },
          {
            hint: 'Cold boxes/vaccine carriers with user-independent freeze prevention features',
            x: 2020,
            y: 20,
            r: 5,
          },
          {
            hint: 'Ebola vaccine stockpile for outbreak response ',
            x: 2021.9,
            y: 60,
            r: 9,
          },
          {
            hint: 'Inactivated polio vaccine supply for oral polio vaccine cessation',
            x: 2023,
            y: 30,
            r: 12,
          },
          {
            hint: 'Chikungunya vaccine stockpile for outbreak response',
            x: 2024.5,
            y: 40,
            r: 13,
          },
          {
            hint: 'Vaccine micro-array patches',
            x: 2024,
            y: 70,
            r: 13,
          },
          {
            hint: 'Marburg vaccine stockpile for outbreak response',
            x: 2028,
            y: 65,
            r: 9,
          }
        ]
      }
    ],
    [
      'Medicines', {
        title: 'Medicines',
        color: '#7f7a7c',
        arr: [
          {
            hint: 'Amoxicillin dispensing envelope',
            x: 2019.6,
            y: 30,
            r: 12,
          },
          {
            hint: 'Cheaper paediatric cancer products in essential medicines lists for middle-income countries',
            x: 2020.5,
            y: 25,
            r: 12,
          },
          {
            hint: 'Oxygen therapy',
            x: 2021,
            y: 80,
            r: 20,
          },
          {
            hint: 'Diabetes treatments',
            x: 2024,
            y: 60,
            r: 20,
          }
        ]
      }
    ],
    [
      'Nutrition', {
        title: 'Nutrition',
        color: '#3b4394',
        arr: [
          {
            hint: 'Alternative formulations of  ready-to-use therapeutic food',
            x: 2023.5,
            y: 80,
            r: 19,
          },
          {
            hint: 'Height/length measurement device',
            x: 2021.4,
            y: 30,
            r: 14,
          }
        ]
      }
    ],
    [
      'Water', {
        title: 'Water, sanitation & hygiene',
        color: '#ea610a',
        arr: [
          {
            hint: 'Household water treatment solutions',
            x: 2021,
            y: 40,
            r: 24,
          },
          {
            hint: 'Low-cost household toilets and related sanitation products in rural and urban settings',
            x: 2023,
            y: 50,
            r: 26,
          },
          {
            hint: 'Rapid E.coli testing device',
            x: 2022,
            y: 70,
            r: 22,
          },
          {
            hint: 'Smart pumps',
            x: 2022.8,
            y: 70,
            r: 14,
          }
        ]
      }
    ],
    [
      'Education', {
        title: 'Education',
        color: '#9b1944',
        arr: [
          {
            hint: 'School furniture & procurement guidelines ',
            x: 2019,
            y: 10,
            r: 10,
          }
        ]
      }
    ],
    [
      'Protection', {
        title: 'Protection',
        color: '#fcc200',
        arr: [
          {
            hint: 'Emergency structures',
            x: 2019.9,
            y: 10,
            r: 9,
          }
        ]
      }
    ],
    [
      'Disability', {
        title: 'Disability',
        color: '#86b826',
        arr: [
          {
            hint: 'Accessible latrine slab add-on',
            x: 2019,
            y: 25,
            r: 9,
          }
        ]
      }
    ],

  ]
)

var bubblesChart = $('.buble_chart_body');
var bubbleControlsBlock = $('.buble_chart_control')

var minYear = $('.buble_chart_body_x_item:first-child').text();
var maxYear = $('.buble_chart_body_x_item:last-child').text();
var rangeYear = maxYear - minYear;

var widthBubbleRatio = $('.buble_chart_body').height()/$('.buble_chart_body').width()

var bubblesList = '';
var bubbleControls = '';

bubles.forEach(function(value, key, map) {

  bubbleControls += '<div class="buble_chart_control_item" data-key="'+key+'">';
    bubbleControls += '<div class="buble_chart_control_item_point" style="background-color: '+value.color+'"></div>';
    bubbleControls += '<div class="buble_chart_control_item_text">'+value.title+'</div>';
  bubbleControls += '</div>';

  // bubbleControls += `<div class="buble_chart_control_item" data-key=${key}>
  //   <div class="buble_chart_control_item_point" style="background-color: ${value.color}"></div>
  //   <div class="buble_chart_control_item_text">${value.title}</div>
  // </div>`;

  bubblesList+= value.arr.map(function(item, index) {

    var style1 = (1 - (maxYear - item.x)/(maxYear - minYear)).toFixed(3)*100;
    var style2 = (item.r * widthBubbleRatio).toFixed(1);
    var str = '<div class="buble jsBubble '+key+'"';
    str += ' style="left:'+style1+'%;';
    str += 'bottom: '+item.y+'%;';
    str += 'height: '+item.r+'%;';
    str += 'width: '+style2+'%;';
    str += 'z-index: '+100 - item.r+';';
    str += 'background-color: '+value.color+'"';
    str += '>';
    str += '<div class="buble_hint">'+item.hint+'</div>';
    str += '</div>';

    // return `<div class="buble jsBubble ${key}"
    //     style="left: ${(1 - (maxYear - item.x)/(maxYear - minYear)).toFixed(3)*100}%;
    //     bottom: ${item.y}%;
    //     height: ${item.r}%;
    //     width: ${(item.r * widthBubbleRatio).toFixed(1)}%;
    //     z-index: ${100 - item.r};
    //     background-color: ${value.color}"
    // >
    //   <div class="buble_hint">${item.hint}</div>
    // </div>`;
    return str;
  })
})

bubblesChart.append(bubblesList);
bubbleControlsBlock.append(bubbleControls);


// bubbles filter

$('.buble_chart_control').on('click', '.buble_chart_control_item', function(e) {
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    //$(`.buble`).removeClass('disable_mod');
    $('.buble').removeClass('disable_mod');
  } else {
    $('.buble_chart_control_item').removeClass('active');
    $(this).addClass('active');
    var bubbleKey = $(this).data('key');
    // $('.buble').addClass('disable_mod');
    // $(`.buble.${bubbleKey}`).removeClass('disable_mod');
    $('.buble').addClass('disable_mod');
    $('.buble.'+bubbleKey).removeClass('disable_mod');
  }
})
;var collabPopup = [
	[
		{
			title: 'Harmonized Approach to Cash Transfers',
			text: 'The United Nations has a common approach to HACT assessments that achieve better solutions, terms and prices from vendors. In 2018, tenders were published in collaboration with FAO, UNDP, UN Women and the United Nations Secretariat to access service providers’ capacity globally but at local prices, via consolidated demand forecasts from multiple organizations and contracting at the global level.'
		}
	],
	[
		{
			title: 'Coordinated vaccine delivery',
			text: 'Through the Measles & Rubella Initiative (MRI), UNICEF supplied measles-containing vaccine and associated devices to 13 countries. Through the International Coordination Group (ICG), UNICEF supplied yellow fever vaccine, oral cholera vaccine and meningococcal vaccine for outbreak response.',
		},
		{
			title: 'Malaria vaccine implementation',
			text: 'In 2018 UNICEF worked with WHO to deliver malaria vaccines to three pilot countries under the Malaria Vaccine Implementation Programme. WHO leads this programme with funding support from the Global Fund, Unitaid and Gavi, the Vaccine Alliance (Gavi).'
		},
	],
	[
		{
			title: 'Mechanisms for vaccine delivery in humanitarian crisis',
			text: 'UNICEF made 85 shipments of vaccines to 19 countries (24.7 million doses) to reach populations on the move that have limited or no access to country health systems, through mechanisms shared with partner organizations such as WHO, Gavi, Global Polio Eradication Initiative, ICG, MRI, MSF and Save the Children.',
		},
		{
			title: 'Expanding warehouse space',
			text: 'For emergency response in the Middle East and North Africa, a partnership with the International Humanitarian City significantly expanded UNICEF’s warehouse space in Dubai. UNICEF also partnered with the Government of Australia to establish a new humanitarian warehouse in Brisbane for response in the Pacific. '
		},
	],
	[
		{
			title: 'Freight forwarding efficiencies',
			text: 'UNICEF has negotiated with international transporters on behalf of 12 United Nations agencies to achieve savings and efficiencies on freight-forwarding service contracts, using competitive tenders to establish direct collaboration and rates with these companies, including demurrage-free periods.'
		}
	],
	[
		{
			title: 'Anti-malarial drugs',
			text: 'UNICEF’s partnerships with PAHO and WHO contributed to a successful first joint tender for anti-malarial drugs. WHO provided the technical expertise, with UNICEF providing the contracting expertise.',
		},
		{
			title: 'Joint United Nations in vitro diagnostics tender',
			text: 'UNICEF leveraged its contracting expertise and worked together with WHO, PAHO, UNDP and UNFPA to launch a United Nations joint tender to establish multiple supply arrangements with all manufacturers of serological assays for HIV and hepatitis B/C, malaria rapid diagnostic tests, HIV virological tests and CD4 technologies.',
		},
		{
			title: 'Generation Unlimited foundational document',
			text: 'UNICEF led the contracting process for the Generation Unlimited Secretariat, a new partnership involving governments, multilateral organizations, civil society and the private sector, formed to support initiatives for children in their crucial second decade of life.'
		},
	],
	[
		{
			title: 'Zika diagnostics',
			text: 'UNICEF, with WHO and USAID, established contracts to incentivize research and development and expand access to new in vitro diagnostics products for Zika disease. UNICEF’s special contracting terms and financing tools help balance supplier and buyer risks in the development and testing of suitable devices and create a path to a viable market.',
		},
		{
			title: 'Testing services for viral diseases',
			text: 'In collaboration with Clinton Health Access Initiative and with Unitaid funds, UNICEF led a tender to establish all-inclusive pricing contracts for nucleic acid testing across HIV, HCV, HBV and HPV, for selected pilot countries. This aims to improve access and availability, and reduce costs, stock-outs, waste and downtime of laboratory testing capacity.'
		}
	],
	[
		{
			title: 'Upgrading the vaccine cold chain',
			text: 'UNICEF leads the implementation of the Cold Chain Equipment Optimization Platform (CCEOP) in collaboration with the Bill & Melinda Gates Foundation, Gavi and WHO. With CCEOP investments, 41 countries are radically transforming the performance and coverage of their cold chains for vaccines, including with energy efficient solar direct-drive refrigerators. '
		}
	],
	[
		{
			title: 'Quality assurance of key products',
			text: 'To strengthen the quality of products procured from suppliers, UNICEF provides free technical assistance in the form of Good Manufacturing Practice inspections. In 2018, a total of 43 manufacturers were assessed. Participating partner organizations in this process include WHO, MSF and WFP. '
		}
	],
	[
		{
			title: 'Sanitation market shaping',
			text: 'In November 2018, UNICEF convened a landmark Sanitation Industry Consultation in Abuja, Nigeria, involving a range of potential stakeholders, including Bill & Melinda Gates Foundation, the World Bank, governments, suppliers and other partners, to influence markets beyond the UNICEF procurement footprint (see page 20).'
		}
	],
	[
		{
			title: 'Piggybacking on existing long-term arrangements for goods and services',
			text: 'UNICEF provides contracts for other United Nations agencies for goods and services: water, sanitation and health supplies, multipurpose tents, power generation equipment, solar energy systems, computer hardware, information and communications technology (ICT) developers, managed firewall services, ICT assistive technologies, laboratory and inspection services, solar electrification consultancies, e-learning platforms, cash transfers and more. To reduce parallel processes and transaction costs, UNICEF uses other agencies’ contracts – e.g. those of UNDP, UNHCR, UNOPS, UNPD, WMO, UN Women, WFP – for goods and services such as vehicles, graphic design and translation, blankets and tarpaulins.'
		}
	]
]


var memberPopup = [
	{
		img_list: [
			'i/members/member_1.jpg',
			'i/members/member_2.jpg',
		],
		credit_list: [
			'©️ UNICEF/Syria/Ali',
			'©️ UNICEF/Eren'
		],
		name: 'Tarek Alakari and Ali Abdallah',
		job: 'Supply & Procurement Officer and Logistics Officer',
		country: 'Syrian Arab Republic',
		text: '“Faced with a constantly-evolving humanitarian supply chain in a complex emergency, our Supply team in the Syrian Arab Republic introduced a business-partnership approach to get supplies to children through cash-based services in collaboration with a range of local vendors. Through the e-Voucher programme, parents can use electronic smart cards charged with credit to freely choose from an agreed list of items which products to buy for their children, with an option to negotiate better prices. It has been inspiring to see how our work has empowered families to act for themselves, upholding their dignity and increasing the programme’s effectiveness. We felt this project was one of the most complex to implement, but the positive response from the families has made the team’s efforts worthwhile.” ',

	},
	{
		img: 'i/members/member_3.jpg',
		credit: '©️ UNICEF/Syria/Abdallah',
		name: 'Sviatlana Kavaliova',
		job: 'Procurement Services and Financing Specialist',
		country: 'Europe and Central Asia Region',
		text: '“The region I work in is unique since it is made-up largely of middle-income countries graduating from international donor support. Our team and I work with governments and partners to strengthen national capacities and ensure that these countries’ health supply financing, procurement and supply chain systems are optimal. This includes strengthening capacity in quantification and forecasting, supply budgeting and planning, reliable data management and the development of partnerships for effective functioning of supply systems. In this way we incorporate nearly all Global Supply Strategies into our work. One of our flagship results is focused on supporting governments in achieving 95 per cent of national diphtheria/pertussis/tetanus vaccine coverage with at least 80 per cent of children immunized with the three doses required in every district. Achieving results like these and being part of a team that finds solutions and is results oriented is what makes my work so exciting and rewarding.”'
	},
	{
		img: 'i/members/member_4.jpg',
		credit: '©️ UNICEF/Efinda',
		name: 'Kyaw Myo Aung',
		job: 'Supply Officer',
		contry: 'Myanmar',
		text: '“UNICEF is working with partners in Myanmar to identify and implement simplifications to business operations, reduce duplication and improve efficiency across the United Nations system by chairing the Procurement Working Group. Using UNICEF’s strategy for science of delivery, our main priority is to establish a common vendor roster via long-term arrangements so that agencies can make informed selections of service modalities and achieve greater value for money when procuring goods and services. Over the course of 2018, our team completed a range of LTAs for common use in Myanmar, including for hygiene kits, transportation services, cash disbursement and event management. It is good to know that thanks to our efforts, we are able to work more closely together with other United Nations agencies to deliver products and services to children across Myanmar.” '
	},
	{
		img: 'i/members/member_5.jpg',
		credit: '©️ UNICEF/Mangoro',
		name: 'Chengetanai Mangoro',
		job: 'Supply Specialist (Procurement Services)',
		country: 'Eastern and Southern Africa Region ',
		text: '“The changing financing landscape places greater emphasis on domestic resources for supplies for children. As UNICEF continues to innovate and evolve to ensure that children continue to access supplies, working with countries to mobilize domestic resources for supplies is a new and challenging field. Progress has been made with countries in identifying the strategic importance of procurement services to maximize the utilization of domestic resources. The greatest reward has been seeing country office colleagues engage proactively with government counterparts and supply financing mechanisms so that resources are used more efficiently.” '
	},
	{
		img: 'i/members/member_6.jpg',
		credit: '©️ UNICEF/Nigeria/Bishen',
		name: 'Amarachi Eboh',
		job: 'Supply and Procurement Associate',
		country: 'Nigeria',
		text: '“In my position I help ensure that the supplies and services procured by the Nigeria Country Office meet the highest standards of quality. We recently contracted a vendor to conduct audits of the national cold chain and vaccine delivery system, and to monitor the vaccine stock management system. I helped evaluate proposals from vendors to ensure that the technical specifications required for the job were met. Finding the right vendor for this work was critical to strengthen national supply chain systems and make sure that active vaccines reach children and keep them healthy.”'
	},
	{
		img: 'i/members/member_7.jpg',
		credit: '©️ UNICEF/Moe_Shw_Syn_Naing',
		name: 'Patrick Efinda',
		job: 'Supply and Logistics Specialist',
		country: 'Denmark',
		text: '“When UNICEF staff and partners are well trained they are able to respond more quickly and efficiently when a disaster hits. In 2018, I was deployed to deal with three very different emergency contexts – an earthquake in Papua New Guinea, an Ebola outbreak in DRC, and a tsunami and earthquake in Indonesia – with the objective to integrate supply procurement and delivery into development work and to strengthen emergency preparedness. My work supported UNICEF’s strategy for the humanitarian development continuum so that countries can more readily respond to future emergencies and recover from them.”'
	},
	{
		img: 'i/members/member_8.jpg',
		credit: '©️ UNICEF/Pakistan/Syed-Tanveer-Hussain-Shah',
		name: 'Imtiaz Ali',
		job: 'Procurement Services Associate',
		country: 'Pakistan',
		text: '“In 2018, I was deployed to Kashmir to conduct a pre-assessment of cold chain and supply readiness in preparation for a national measles vaccination campaign. It was a great learning experience to monitor the deployment of Cold Chain Equipment Optimization Platform equipment in remote and conflict-affected areas in preparation for the campaign. I visited more than 150 health facilities and used the Rapid Pro application to capture data on site readiness and vaccines delivered. More than 50 per cent of cold chain equipment was obsolete and many health facilities were using domestic fridges for vaccine storage. Deployment of cold chain equipment enhanced storage capacity, significantly increasing our ability to reach 95 per cent of children with vaccines. Last-mile end user monitoring increased my understanding of the new platform as an entry point for national supply chain systems strengthening and to address equity and systems design.”'
	}
]

var partnershipPopup = [
	{
		title: 'United Nations family',
		text: [
			'Food and Agriculture Organization of the United Nations (FAO)',
			'High-Level Committee on Management Procurement Network (HLCM PN)',
			'International Organization for Migration (IOM)',
			'Joint United Nations Programme on HIV/AIDS (UNAIDS)',
			'Medicines Patent Pool (MPP)',
			'Pan American Health Organization (PAHO)',
			'The World Bank (WB)',
			'United Nations Development Programme (UNDP)',
			'United Nations Framework Convention on Climate Change (UNFCCC)',
			'United Nations Global Logistics Cluster',
			'United Nations Global Service Centre (UNGSC)',
			'United Nations Global WASH Cluster',
			'United Nations Humanitarian Response Depot (UNHRD)',
			'United Nations Office for Project Services (UNOPS)',
			'United Nations Office for the Coordination of Humanitarian Affairs (OCHA)',
			'United Nations System Staff College (UNSSC)',
			'United Nations Population Fund (UNFPA)',
			'United Nations Procurement Division (UNPD)',
			'United Nations Refugee Agency (UNHCR)',
			'United Nations Relief and Works Agency for Palestine Refugees (UNRWA)',
			'World Food Programme (WFP)',
			'World Health Organization (WHO)'
		]
	},
	{
		title: 'Donor governments and international financial institutions',
		text: [
			'African Development Bank (AfDB)',
			'Global Affairs Canada (formerly CIDA)',
			'Centers for Disease Control and Prevention (CDC), United States',
			'Department for International Development (DFID), United Kingdom',
			'European Commission’s Humanitarian Aid and Civil Protection (ECHO)',
			'Islamic Development Bank (IDB)',
			'International Humanitarian City (IHC), Dubai',
			'Japan International Cooperation Agency (JICA)',
			'Kreditanstalt für Wiederaufbau (KfW), Germany',
			'President’s Malaria Initiative (PMI), United States',
			'United States Agency for International Development (USAID)',
			'United States Department of Agriculture (USDA) ',
			'United States Office of the Global AIDS Coordinator (OGAC)',
			'The United States President’s Emergency Plan for AIDS Relief (PEPFAR)'
		]
	},
	{
		title: 'Academia, foundations and businesses',
		text: [
			'Bill & Melinda Gates Foundation (BMGF)',
			'Chemonics',
			'Clinton Health Access Initiative (CHAI)',
			'Developing Countries Vaccine Manufacturers Network (DCVMN)',
			'HP Inc.',
			'International Federation of Pharmaceutical Manufacturers & Associations (IFPMA)',
			'John Snow, Inc. (JSI)',
			'la Caixa Foundation',
			'London School of Hygiene & Tropical Medicine',
			'Lugano University (Università della Svizzera Italiana)',
			'Norwegian Airlines',
			'Panalpina',
			'Schmidt Futures',
			'The LEGO Foundation ',
			'The UPS Foundation',
			'United Nations Foundation',
			'United States Pharmacopeial Convention (USP)',
			'University of Copenhagen',
			'University of Oxford'
		]
	},
	{
		title: 'Civil society',
		text: [
			'Action Contre la Faim (ACF)',
			'East Europe and Central Asia Union of People Living with HIV (ECUO)',
			'Elizabeth Glaser Pediatric AIDS Foundation (EGPAF)',
			'Humanitarian Innovation Fund (HIF) - Elrha',
			'International Committee of the Red Cross (ICRC)',
			'International Federation of Red Cross and Red Crescent Societies (IFRC)',
			'International Rescue Committee (IRC)',
			'Learn4Dev',
			'Médecins sans Frontières (MSF)',
			'Nutrition International',
			'Oxfam',
			'PATH',
			'Population Services International (PSI)',
			'Save the Children International',
			'VillageReach',
			'World Vision International'
		]
	},
	{
		title: 'Health and supply chain partnerships',
		text: [
			'African Leaders Malaria Alliance (ALMA)',
			'African Network for Drugs and Diagnostics Innovation (ANDI)',
			'African Society for Laboratory Medicine (ASLM)',
			'Coalition for Epidemic Preparedness Innovations (CEPI)',
			'Eastern Mediterranean Public Health Network (EMPHNET)',
			'Foundation for Innovative New Diagnostics (FIND)',
			'Gavi, the Vaccine Alliance (Gavi)',
			'Global Diagnostics Working Group',
			'Global Drug Facility',
			'Global Financing Facility',
			'Global Polio Eradication Initiative (GPEI)',
			'Innovation to Impact (I2I)',
			'Interagency Supply Chain Group',
			'Learning Network for Countries in Transition (LNCT)',
			'Measles & Rubella Initiative (MRI)',
			'Medicines for Malaria Venture',
			'People that Deliver (PtD)',
			'Pharmaceutical Inspection Co-operation Scheme (PIC/S)',
			'Roll Back Malaria (RBM) Partnership',
			'The Alliance for Malaria Prevention (AMP)',
			'The Global Fund',
			'Unitaid'
		]
	}
]

var countries = [
	{
		name: 'France',
		data: '190.4'
	},
	{
		name: 'India',
		data: '529.6'
	},
	{
		name: 'United States',
		data: '485.5',
	},
	{
		name: 'Belgium',
		data: '380.5'
	},

	{
		name: 'Yemen',
		data: '130.8'
	},
	{
		name: 'Denmark',
		data: '123.4'
	},
	{
		name: 'Pakistan',
		data: '111.2'
	},
	{
		name: 'United Kingdom',
		data: '91.8'
	},
	{
		name: 'Netherlands',
		data: '71.3'
	},
	{
		name: 'China',
		data: '68.9'
	},
	{
		name: 'Republic of Korea',
		data: '59.2'
	},
	{
		name: 'Kenya',
		data: '53'
	},
	{
		name: 'Luxembourg',
		data: '49.4'
	},
	{
		name: 'Nigeria',
		data: '48'
	},
	{
		name: 'Russian Federation',
		data: '47.2'
	},
	{
		name: 'Syrian Arab Republic',
		data: '44.1'
	},
	{
		name: 'Lebanon',
		data: '41.9'
	},
	{
		name: 'South Sudan',
		data: '39.4'
	},
	{
		name: 'Afganistan',
		data: '38.1'
	},
	{
		name: 'Democratic Republic of the Congo',
		data: '36.7'
	},
	{
		name: 'Germany',
		data: '36.3'
	},
	{
		name: 'Ethiopia',
		data: '35.6 '
	},
	{
		name: 'South Africa',
		data: '34.3'
	},
	{
		name: 'Singapore',
		data: '33.7'
	},
	{
		name: 'Bangladesh',
		data: '30.3'
	},
	{
		name: 'Jordan',
		data: '30'
	},
	{
		name: 'Switzerland',
		data: '29.8'
	},
	{
		name: 'Indonesia',
		data: '29.7'
	},
	{
		name: 'Turkey',
		data: '28.9'
	},
	{
		name: 'Iraq',
		data: '28.2'
	},
	{
		name: 'Niger',
		data: '22.8'
	},
	{
		name: 'Sudan',
		data: '22'
	}
]

var savings = [
	{
		year: '2018',
		target: 260,
		above: 91.2
	},
	{
		year: '2019',
		target: 290
	},
	{
		year: '2020',
		target: 70
	},
	{
		year: '2021',
		target: 70
	}
]
;	// countries graph

	var countriesChart = $('.countries_chart');

	countries.sort(function(a, b) {
    return b.data - a.data;
	});

	var maxCountriesData = countries[0].data;


	var countriesBlock = countries.map(function(item, index) {
		var height = item.data/maxCountriesData * 100;
		var out = '<div class="chart_country_item" style="height: '+height.toFixed(1)+'%">';
		out += '<div class="chart_country_data">$ '+item.data+' million</div>';
		out += '<div class="chart_country_bar"></div>';
		out += '<div class="chart_country_name">'+item.name+'</div>';
		out += '</div>';

		return out;
		// return `<div class="chart_country_item" style="height: ${height.toFixed(1)}%">
		// 	<div class="chart_country_data">$ ${item.data} million</div>
		// 	<div class="chart_country_bar"></div>
		// 	<div class="chart_country_name">${item.name}</div>
		// </div>`
	});

	countriesChart.append(countriesBlock);

	// saving graph

	var savingChart = $('.saving_chart');

	var valsSaving = savings.map(function(item, index) {
		var above = item.above || 0;
		var target = item.target || 0;
		return above + target;
	});

	//var maxSaving = Math.max(...valsSaving);
	var maxSaving = Math.min.apply(Math, valsSaving);

	var  maxHeight = (maxSaving/100).toFixed(0)*100;
	var step = maxHeight/4;

	var currentStep = 0;

	for (var i=0; i<5; i++) {
		currentStep= step*i;
		var val = maxHeight - currentStep;
		$('.saving_graph').append('<div class="saving_graph_item">$'+val+'</div>');
		// $('.saving_graph').append(`<div class="saving_graph_item">$${maxHeight - currentStep}</div>`)
	}

	var savingBlocks = savings.map(function(item, index) {
		var height1 = item.target/maxHeight * 100 || 0;
		var height2 = item.above/maxHeight * 100 || 0;
		var above = item.above || '';

		var out = '<div class="chart_saving_item">';
		out += '<div class="chart_saving_bar above_mod" style="height: '+height2.toFixed(1)+'%">'+above+'</div>';
		out += '<div class="chart_saving_bar total_mod" style="height: '+height1.toFixed(1)+'%">'+item.target+'</div>';
		out += '<div class="chart_saving_year">'+item.year+'</div>';
		out += '</div>';

		return out;
		// return `<div class="chart_saving_item">
		// 	<div class="chart_saving_bar above_mod" style="height: ${height2.toFixed(1)}%">${item.above || ''}</div>
		// 	<div class="chart_saving_bar total_mod" style="height: ${height1.toFixed(1)}%">${item.target}</div>
		// 	<div class="chart_saving_year">${item.year}</div>
		// </div>`
	});

	savingChart.append(savingBlocks);
;var Tabs =
/*#__PURE__*/
function () {
  function Tabs($tabsWrap) {
    _classCallCheck(this, Tabs);

    this.$tabsWrap = $tabsWrap;
    this.$switches = this.$tabsWrap.find('[data-switch]');
    this.$targets = this.$tabsWrap.find('[data-target]');
    this.lock = true;

    this._init();
  }

  _createClass(Tabs, [{
    key: "_init",
    value: function _init() {
      var _this = this;

      this._turnOnTarget();

      this.$switches.on('click', function (e) {
        if (!_this.lock) {
          return false;
        }

        var $curSwitch = $(e.currentTarget);

        _this._turnOffSwitch();

        _this._turnOnSwitch($curSwitch);

        _this._turnOffTarget();

        _this._turnOnTarget();
      });
    }
  }, {
    key: "_turnOffSwitch",
    value: function _turnOffSwitch() {
      this.$switches.filter('.active_mod').removeClass('active_mod');
    }
  }, {
    key: "_turnOnSwitch",
    value: function _turnOnSwitch($curSwitch) {
      $curSwitch.addClass('active_mod');
    }
  }, {
    key: "_turnOffTarget",
    value: function _turnOffTarget() {
      var _this2 = this;

      var $activeTarget = this.$targets.filter('.active_mod');
      new TimelineMax({
        onStart: function onStart() {
          $activeTarget.removeClass('active_mod');
          _this2.lock = false;
        },
        onComplete: function onComplete() {
          _this2.lock = true;
        }
      }).to($activeTarget, .4, {
        opacity: 0,
        clearProps: 'all'
      });
    }
  }, {
    key: "_turnOnTarget",
    value: function _turnOnTarget() {
      var _this3 = this;

      new TimelineMax({
        onStart: function onStart() {
          _this3.curTarget.addClass('active_mod');

          _this3.lock = false;
        },
        onComplete: function onComplete() {
          _this3.lock = true;
        }
      }).from(this.curTarget, .4, {
        opacity: 0,
        clearProps: 'all'
      });
    }
  }, {
    key: "reset",
    value: function reset() {
      this._turnOffSwitch();

      this._turnOnSwitch(this.$switches.eq(0));

      this.$targets.removeClass('active_mod');
      this.$targets.eq(0).addClass('active_mod');
    }
  }, {
    key: "curTarget",
    get: function get() {
      var activeNumber = this.$switches.filter('.active_mod').data('switch');
      return this.$targets.filter("[data-target=".concat(activeNumber, "]"));
    }
  }]);

  return Tabs;
}();
;var TimeLinePopup =
/*#__PURE__*/
function () {
  function TimeLinePopup($popupWrap, $openBtns, contentCollection) {
    _classCallCheck(this, TimeLinePopup);

    this.$popupWrap = $popupWrap;
    this.$openBtns = $openBtns;
    this.contentCollection = contentCollection;
    this.actualTarget;
    this.curContent;

    this._init();
  }

  _createClass(TimeLinePopup, [{
    key: "_init",
    value: function _init() {
      var _this4 = this;

      var $closeBtn = this.$popupWrap.find('.jsCloseBtn');
      this.$openBtns.on('click', function (e) {
        _this4.actualTarget = $(e.currentTarget).data('decade');

        _this4._changeContent();

        _this4._open();
      });
      $closeBtn.on('click', function () {
        _this4._close();

        $('body').removeClass('popup_mod');

        _this4.photo.attr('src', '');
      });
    }
  }, {
    key: "_open",
    value: function _open() {
      this.$popupWrap.addClass('open_mod');
      $('body').addClass('popup_mod');
    }
  }, {
    key: "_close",
    value: function _close() {
      this.$popupWrap.removeClass('open_mod');
    }
  }, {
    key: "_changeContent",
    value: function _changeContent() {
      this.decade.html("".concat(this.actualContent.decade, "s:"));
      this.title.html(this.actualContent.title);
      this.text.html(this.actualContent.text);
      this.photoDescr.html(this.actualContent.photoDescr);
      this.photo.attr('src', this.actualContent.photo);
    }
  }, {
    key: "actualContent",
    get: function get() {
      return this.contentCollection.get(this.actualTarget);
    }
  }, {
    key: "decade",
    get: function get() {
      return this.$popupWrap.find('.jsDecade');
    }
  }, {
    key: "title",
    get: function get() {
      return this.$popupWrap.find('.jsTitle');
    }
  }, {
    key: "text",
    get: function get() {
      return this.$popupWrap.find('.jsText');
    }
  }, {
    key: "photoDescr",
    get: function get() {
      return this.$popupWrap.find('.jsPhotoDescr');
    }
  }, {
    key: "photo",
    get: function get() {
      return this.$popupWrap.find('.jsPhoto');
    }
  }]);

  return TimeLinePopup;
}();
;var WorldMap =
/*#__PURE__*/
function () {
  function WorldMap($mapWrap) {
    _classCallCheck(this, WorldMap);

    this.$mapWrap = $mapWrap;
    this.$triggers = $mapWrap.find('[data-target]');
    this.$countries = $mapWrap.find('.world_map_country');
    this.$points = $mapWrap.find('.world_map_point');
    this.$lines = $mapWrap.find('.world_map_line');
    this.$textes = $mapWrap.find('.world_map_text');
    this.active = '';

    this._init();
  }

  _createClass(WorldMap, [{
    key: "_init",
    value: function _init() {
      var _this5 = this;

      this.$triggers.on('click', function (e) {
        //console.log( $(e.currentTarget).data('target') );
        if (_this5.active == '' || _this5.active != $(e.currentTarget).data('target')) {
          var $curTrigger = $(e.currentTarget);
          _this5.active = $(e.currentTarget).data('target');

          _this5.$triggers.addClass('off_mod');

          $curTrigger.removeClass('off_mod');

          _this5._turnOffTarget();

          _this5._turnOnTarget($curTrigger.data('target'));
        } else if (_this5.active == $(e.currentTarget).data('target')) {
          _this5.active = '';

          _this5.$triggers.removeClass('off_mod');

          _this5._turnOn();
        }
      });
    }
  }, {
    key: "_turnOn",
    value: function _turnOn() {
      this.$countries.removeClass('off_mod');
      this.$points.removeClass('off_mod');
      this.$lines.removeClass('off_mod');
      this.$textes.removeClass('off_mod');
    }
  }, {
    key: "_turnOnTarget",
    value: function _turnOnTarget(targetMod) {
      var curMod = "".concat(targetMod, "_mod");
      var $active = $(".".concat(curMod));
      $active.removeClass('off_mod');
    }
  }, {
    key: "_turnOffTarget",
    value: function _turnOffTarget() {
      this.$countries.addClass('off_mod');
      this.$points.addClass('off_mod');
      this.$lines.addClass('off_mod');
      this.$textes.addClass('off_mod');
    }
  }]);

  return WorldMap;
}();
;function addNewContent($wrap, newContent) {
  $name = $wrap.find('.jsName');
  $products = $wrap.find('.jsDataProduct');
  $dataIcons = $wrap.find('.jsDataIcon');
  $dataAmounts = $wrap.find('.jsDataAmount');
  $dataProdNames = $wrap.find('.jsDataProdName');
  $dataAmounts2 = $wrap.find('.jsDataAmount2');
  $dataTexts = $wrap.find('.jsDataText');
  $text = $wrap.find('.jsText');
  $photo = $wrap.find('.jsPhoto');
  $credit = $wrap.find('.jsCredit');
  $name.html(newContent.regName);
  $text.html(newContent.text);
  $photo.attr('src', newContent.photo);
  $credit.html(newContent.credit);

  for (var _i = 0; _i < newContent.data.length; _i++) {
    var el = newContent.data[_i];
    $dataIcons.eq(_i).attr('src', el.icon);
    $dataAmounts.eq(_i).html(el.amount);
    $dataProdNames.eq(_i).html(el.prodName);
    $dataAmounts2.eq(_i).html(el.amount2);
  }
}

function popupSliderInit() {
  $('.region_item_data_row.v1_mod').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    responsive: [{
      breakpoint: 767,
      settings: "unslick"
    }]
  });
}

function popupSliderDestroy() {
  $('.region_item_data_row.v1_mod').slick("unslick");
}

var WorldMapPopup =
/*#__PURE__*/
function () {
  function WorldMapPopup($popupWrap, $openBtns, contentCollection) {
    _classCallCheck(this, WorldMapPopup);

    this.$popupWrap = $popupWrap;
    this.$openBtns = $openBtns;
    this.contentCollection = contentCollection;
    this.actualItemId;
    this.curContent;
    this.WorldMapTabs;

    this._afterCloseCallback = function () {
      return false;
    };

    this._init();
  }

  _createClass(WorldMapPopup, [{
    key: "_init",
    value: function _init() {
      var _this6 = this;

      var $closeBtn = this.$popupWrap.find('.jsCloseBtn');
      this.$openBtns.on('click', function (e) {
        _this6.actualReg = $(e.currentTarget).data('region');

        _this6._changeContent(_this6.actualReg);

        _this6._open();

        popupSliderInit();
      });
      $closeBtn.on('click', function () {
        _this6._close();

        popupSliderDestroy();
        $('body').removeClass('popup_mod');
      });
    }
  }, {
    key: "_open",
    value: function _open() {
      this.$popupWrap.addClass('open_mod');
      $('body').addClass('popup_mod');
    }
  }, {
    key: "_close",
    value: function _close() {
      this.$popupWrap.removeClass('open_mod');

      this._afterCloseCallback();
    }
  }, {
    key: "_changeContent",
    value: function _changeContent(countryName) {
      addNewContent(this.$popupWrap, this.contentCollection.get(countryName));
    }
  }, {
    key: "onAfterClose",
    value: function onAfterClose(callback) {
      this._afterCloseCallback = callback;
    }
  }]);

  return WorldMapPopup;
}();
