var $body,
	$header,
	$scrollTop = 0,
	windowHeight,
	windowWidth,
	$menuTrigger,
	$menuTriggerHamburger,
	$menuTriggerTitle,
	$menu,
	$chaptersBlock,
	$chaptersImg,
	$chaptersSlider,
	$chaptersImgSlider,
	indexPopup,
	contentPopup,
	$graphTabsLink,
	$graphTabsBlock,
	imagesPopup,
	textPopup,
	$popupJsBlock,
	$logo,
	degree = 0.0174532925,
	mediaPoint1 = 1024,
	mediaPoint2 = 768,
	mediaPoint3 = 480,
	mediaPoint4 = 320;


	var decadesContent = new Map([[1940, {
  decade: 1940,
  title: 'The essentials',
  text: '<p>UNICEF was founded in December 1946 at the United Nations General Assembly. In the immediate aftermath of the Second World War, the organization procured basic survival essentials such as milk and clothing for children affected by the conflict. The mass purchase of these basic off-the-shelf products marked the beginning of UNICEF’s engagement with business. All children were treated equally, establishing from the outset that UNICEF was there to help every child.</p>',
  photoDescr: 'Photo: Children in the Philippines receive milk after the Second World War</br>©️ UNICEF/UNI43118/Unknown',
  photo: 'i/timeline/photo_1940.jpg'
}], [1950, {
  decade: 1950,
  title: 'The midwifery kit',
  text: '<p>In a small village in the Philippines in the 1950s, a UNICEF midwife who was training local midwives became frustrated that new supplies had to be ordered one by one from Manila. Wouldn’t it be easier if all the materials needed by one midwife could be supplied in a pre-packaged, complete kit? She passed her idea on to her supervisor and the UNICEF midwifery kit was created soon after.</p>',
  photoDescr: 'Photo: Midwives in Iran bathe new-borns</br>©️ UNICEF/UNI43118/Unknown',
  photo: 'i/timeline/photo_1950.jpg'
}], [1970, {
  decade: 1970,
  title: 'Water pumps',
  text: '<p>A hand-powered water pump can make clean drinking water available by drawing it from underground. In 1975, UNICEF, WHO and the Government of India recognized the need for a better pump than was available on the market. By 1984 after extensive UNICEF engagement with the industry to promote product development, 36 companies were producing 100,000 of the new pumps every year. Today, the India Mark II and III are the most widely used hand pumps in the world.</p>',
  photoDescr: 'Photo: Boy in India at water pump</br>©️ UNICEF/UNI93877/Biswas',
  photo: 'i/timeline/photo_1970.jpg'
}], [19801, {
  decade: 1980,
  title: 'Oral rehydration salts',
  text: '<p>Oral rehydration salts (ORS) and zinc are a cost-effective treatment for childhood diarrhoea, a leading cause of death in children under five. In the 1980s, UNICEF launched a major effort to expand the use of ORS to save children’s lives. UNICEF suppliers have been required since 2006 to offer an ORS formula with adjusted salt concentrations to improve its effectiveness. In 2014, UNICEF further enhanced diarrhoea treatment by co-packaging ORS with zinc, which reduces the disease’s severity.</p>',
  photoDescr: 'Photo: A child in China receiving ORS</br>©️ UNICEF Guatemala/2018-016/R. Mussapp',
  photo: 'i/timeline/photo_1980.jpg'
}], [19802, {
  decade: 1980,
  title: 'Backpacks',
  text: '<p>When children have the opportunity and the right supplies to succeed in school, their personal development and mental well-being benefit. UNICEF’s iconic school backpack provides them with all the space they need to carry books, food and personal items. The product specifications are adaptable to local needs and context. Recent developments have strengthened the quality of the bags and made them more durable and long-lasting.</p>',
  photoDescr: 'Photo: Girls in Guatemala with new backpacks</br>©️ UNICEF/UNI54378/LeMoyne',
  photo: 'i/timeline/photo_19802.jpg'
}], [19901, {
  decade: 1990,
  title: 'School-in-a-box',
  text: '<p>One of UNICEF’s most well-known products is the School-in-a-box, the idea for which came from a staff member. It was first used after the Rwandan genocide to quickly provide the tools to support learning. It contains a range of essential school supplies for a teacher and 40 children, each of whom receive their own notebook, slate, pen and pencil. The inside lid of the box can be used as a blackboard. For the latest iteration of the box, UNICEF has worked with business to apply Universal Design principles to help children with disabilities: for example, by adding tactile features to globes and plasticised posters, a magnifying glass to rulers, and teaching clocks with braille.</p>',
  photoDescr: 'Photo: A woman opening a School-in-a-box in Nigeria</br>©️ UNICEF/UNI190537/Quarmyne',
  photo: 'i/timeline/photo_19901.jpg'
}], [19902, {
  decade: 1990,
  title: 'Vaccine vial monitors',
  text: '<p>UNICEF procures billions of doses of life-saving vaccines each year, all of which need to be kept within a temperature range to maintain potency. The vaccine vial monitor (VVM) is a small circle that is placed or printed on a vaccine vial and changes colour as it is exposed to heat. Health workers rely on the monitors to determine whether a vaccine will still be potent and effective. Since 2004, VVMs have become part of UNICEF’s minimum standards for all vaccine purchases. By fully adopting this technology, UNICEF has helped health workers all over the world make informed decisions about the viability of the vaccines they are administering.</p>',
  photoDescr: 'Photo: Vial of polio vaccine with VVM in Ghana</br>©️ UNICEF/ UNI193697/Esiebo',
  photo: 'i/timeline/photo_19902.jpg'
}], [2000, {
  decade: 2000,
  title: 'Ready-to-use therapeutic food',
  text: '<p>UNICEF first procured this life-saving nutritional paste from just one supplier in 2000, the originating manufacturer. A strategic decision was then taken to support new market entrants so that the base of suppliers meeting UNICEF quality standards would expand to support capacity increase, including close to beneficiaries. By the end of 2018, nearly two thirds of the volume of the ready-to-use therapeutic food procured by UNICEF came from countries where UNICEF implements programmes. This has reduced the time it takes to reach children, potentially contributed to local economies and established a more sustainable market with a reduced carbon footprint for this strategic product.</p>',
  photoDescr: 'Photo: A child in Chad eating RUTF</br>©️ UNICEF/Vincent_Trémeau',
  photo: 'i/timeline/photo_2000.jpg'
}], [2010, {
  decade: 2010,
  title: 'Personal protective equipment',
  text: 'The 2014–2016 Ebola outbreak in west Africa resulted in nearly 30,000 people being infected and caused more than 10,000 deaths. Response personnel in the affected countries wore personal protective equipment (PPE) to prevent the spread of the disease. UNICEF convened a two-day PPE industry consultation in 2015 to consider ways to make the equipment safer. As a result of the discussions, manufacturers improved product designs in real time, while the outbreak was ongoing. Improvements included tighter weaving to stop the virus from entering clothing.',
  photoDescr: 'Photo: Health workers wearing PPE in Sierra Leone</br>©️ UNICEF/UNI178352/Naftalin ',
  photo: 'i/timeline/photo_2010.jpg'
}]]);
var regionsCollection = new Map([['venezuela', {
  regName: 'Bolivarian Republic of Venezuela migration crisis',
  data: [{
    icon: 'i/products/diagnostic-test.svg',
    amount: '$3.4 million',
    prodName: 'Diagnostic test kits',
    amount2: '$12.4 m'
  }, {
    icon: 'i/products/nutrition.svg',
    amount: '$2.3 million',
    prodName: 'Nutrition supplies',
    amount2: '$13.1 million'
  }, {
    icon: 'i/products/vaccines.svg',
    amount: '$2.3 million',
    prodName: 'Vaccines/biologicals',
    amount2: '$0.7 m'
  }],
  text: 'In 2018, UNICEF established an emergency logistics operation in Venezuela to prepare for an expected worsening of the situation. The new arrangements included local customs clearance services for both sea and air, rental of warehouse facilities and contracting of in-country transportation for supplies. UNICEF also responded in neighbouring countries such as Brazil, Colombia, Ecuador and Peru to accommodate Venezuelan refugees.',
  photo: 'i/products/venezuela.jpg',
  credit: 'José David, 9 years old, is accompanied by his mother to get his vaccination at a UNICEF health point in Ipiales, Colombia for children and families displaced from Venezuela. <br/> © UNICEF/UN0253234/Moreno Gonzalez'
}], ['syria', {
  regName: 'Syrian Arab Republic and the sub-region',
  data: [{
    icon: 'i/products/books.svg',
    amount: '$17.7 million',
    prodName: 'Education supplies',
    amount2: '$46.7 m'
  }, {
    icon: 'i/products/vaccines.svg',
    amount: '$13.1 million',
    prodName: 'Vaccines/biologicals',
    amount2: '$91.3 million'
  }, {
    icon: 'i/products/clothing.svg',
    amount: '$12.7 million',
    prodName: 'Clothing and footwear',
    amount2: '$44.6 m'
  }],
  text: 'With the conflict now in its eighth year, more than six million people remained displaced within Syria and more than five million were refugees in the neighbouring countries of Egypt, Iraq, Jordan, Lebanon and Turkey. In addition to education, immunization and winterization supplies, the parents of 28,000 children in Syria received virtual cash through a UNICEF eVouchers programme. This allows them to select from a range of goods in preselected stores, giving them some freedom to choose the supplies they most need.',
  photo: 'i/products/syria.jpg',
  credit: 'A girl sits in a tent school where 350 children between the ages of 7 and 14 are able to go back to learning at Junaina makeshift camp in Idlib, Syrian Arab Republic. <br/> © UNICEF/UN0248435/Watad'
}], ['nigeria', {
  regName: 'Nigeria',
  data: [{
    icon: 'i/products/nutrition.svg',
    amount: '$14.2 million',
    prodName: 'Nutrition supplies',
    amount2: '$22.2 m'
  }, {
    icon: 'i/products/med-kit.svg',
    amount: '$5.6 million',
    prodName: 'Medical equipment',
    amount2: '$26.9 million'
  }, {
    icon: 'i/products/water.svg',
    amount: '$2 million',
    prodName: 'Water & sanitation',
    amount2: '$4.7 m'
  }],
  text: 'Levels of insecurity and hostilities in the conflict in Nigeria’s Northeastern State increased in 2018, putting children at risk of violence, malnutrition and displacement. Despite this, UNICEF was able to deliver water and sanitation supplies and medical equipment, as well as RUTF to reach 200,000 children affected by malnutrition. UNICEF provided capacity building to strengthen the national supply chain system and to more efficiently reach vulnerable children with supplies.',
  photo: 'i/products/nigeria.jpg',
  credit: 'A young mother and her child at a UNICEF-supported mobile mother, new-born and child health clinic in Zamfara State, Nigeria. <br/> © UNICEF/UN0270161/Knowles-Coursin'
}], ['car', {
  regName: 'Central African Republic',
  data: [{
    icon: 'i/products/nutrition.svg',
    amount: '$2 million',
    prodName: 'Nutrition supplies',
    amount2: '$6.9 m'
  }, {
    icon: 'i/products/vaccines.svg',
    amount: '$2 million',
    prodName: 'Vaccines/biologicals',
    amount2: '$8.9 million'
  }, {
    icon: 'i/products/shelter.svg',
    amount: '$1.5 million',
    prodName: 'Shelter/field equipment',
    amount2: '$2 m'
  }],
  text: 'After six years of conflict, nearly 1.3 million children needed humanitarian assistance in 2018. UNICEF’s emergency response was supported with supplies to treat malnutrition, provide routine immunization and set up temporary learning and protection spaces. UNICEF has overcome a range of challenges in the evolving conflict to continue reaching children in remote and inaccessible areas.',
  photo: 'i/products/car.jpg',
  credit: 'A nurse with her daughter after vaccinating children in the village of Salanga in the Central African Republic. <br/> © UNICEF/UN0226717/Njiokiktjien VII Photo'
}], ['congo', {
  regName: 'Democratic Republic of the Congo',
  data: [{
    icon: 'i/products/water.svg',
    amount: '$0.9 million',
    prodName: 'Water & sanitation',
    amount2: '$4 m'
  }, {
    icon: 'i/products/shelter.svg',
    amount: '$0.8 million',
    prodName: 'Shelter/field equipment',
    amount2: '$7.7 million'
  }, {
    icon: 'i/products/med-kit.svg',
    amount: '$0.4 million',
    prodName: 'Medical equipment',
    amount2: '$3.7 m'
  }],
  text: 'Among various other emergencies, the country faced two Ebola outbreaks in 2018, one of which was the world’s second-largest. To help reduce transmission of this deadly virus, UNICEF shipped chlorine for cleaning and hand washing, non-contact infrared thermometers to track fever and other specialist equipment for Ebola treatment centres, to a total value of $3.1 million. Previous analysis of key supplies required for Ebola response allowed UNICEF to make proactive decisions to procure and pre-position essential products in advance of a possible further spread of the outbreaks.',
  photo: 'i/products/congo.jpg',
  credit: 'A student washes her hands before class in the school yard of Marie Madeleine Primary School in the Democratic Republic of the Congo. <br/> © UNICEF/UN0271326/Tremeau'
}], ['iraq', {
  regName: 'Iraq',
  data: [{
    icon: 'i/products/water.svg',
    amount: '$10.1 million',
    prodName: 'Water & sanitation',
    amount2: '$2.2 m'
  }, {
    icon: 'i/products/clothing.svg',
    amount: '$5.1 million',
    prodName: 'Clothing and footwear',
    amount2: '$23.4 million'
  }, {
    icon: 'i/products/books.svg',
    amount: '$2.5 million',
    prodName: 'Education supplies',
    amount2: '$21.2 m'
  }],
  text: 'UNICEF provided critical support to 4 million returnees, 1.7 million internally displaced persons and approximately 300,000 refugees from other countries with a wide range of products procured throughout the year. These included water, sanitation and hygiene items, clothes for the winter season and education supplies such as prefabricated classrooms and education kits.',
  photo: 'i/products/iraq.jpg',
  credit: 'Mujbal and Mutaab, brothers, use a water tank near their tent in a displacement camp in Anbar Province, Iraq. <br/> © UNICEF/UN0203985/Jeelo'
}], ['yemen', {
  regName: 'Yemen',
  data: [{
    icon: 'i/products/vaccines.svg',
    amount: '$42.9 million',
    prodName: 'Vaccines/biologicals',
    amount2: '$92.9 m'
  }, {
    icon: 'i/products/water.svg',
    amount: '$31.6 million',
    prodName: 'Water & sanitation',
    amount2: '$135.6 million'
  }, {
    icon: 'i/products/nutrition.svg',
    amount: '$21.8 million',
    prodName: 'Nutrition supplies',
    amount2: '$42.7 m'
  }],
  text: 'The conflict in Yemen continues to be the largest humanitarian crisis in the world with 22 million people affected, half of whom are children. In 2018, UNICEF chartered 15 flights for the urgent delivery of immunization, water and sanitation, and nutrition supplies. A cash transfer project has reached nearly 9 million Yemenis since its inception in August 2017, giving them unconditional choice over their daily purchases, in lieu of direct aid that can be challenging to deliver.',
  photo: 'i/products/yemen.jpg',
  credit: 'A child is vaccinated against cholera in Al Marawi’ah District, Yemen. <br/> © UNICEF/UN0240909/Saeed'
}], ['bangladesh', {
  regName: 'Bangladesh',
  data: [{
    icon: 'i/products/water.svg',
    amount: '$4 million',
    prodName: 'Water & sanitation',
    amount2: '$13.1 m'
  }, {
    icon: 'i/products/books.svg',
    amount: '$1.9 million',
    prodName: 'Education supplies',
    amount2: '$18.6 million'
  }, {
    icon: 'i/products/med-kit.svg',
    amount: '$1.4 million',
    prodName: 'Medical equipment',
    amount2: '$5.5 m'
  }],
  text: 'In 2018, more than 730,000 Rohingya, including approximately 400,000 children, were living in refugee camps in Bangladesh, after escaping persecution and violence across the border in Myanmar. Throughout the year, UNICEF supported both displaced children and those from host communities with a range of supplies. Crucial to the success of the operation was forward planning to prepare for logistical challenges caused by the changing seasons.',
  photo: 'i/products/bangladesh.jpg',
  credit: 'A Rohingya refugee child washes at a water well near her family’s shelter in a refugee camp in Cox’s Bazaar District, Bangladesh. <br/> © UNICEF/UN0185020/Sokol'
}], ['indonesia', {
  regName: 'Indonesia',
  data: [{
    icon: 'i/products/water.svg',
    amount: '$1.3 million',
    prodName: 'Water & sanitation',
    amount2: '$2.4 m'
  }, {
    icon: 'i/products/shelter.svg',
    amount: '$1 million',
    prodName: 'Shelter/field equipment',
    amount2: '$3.5 million'
  }, {
    icon: 'i/products/books.svg',
    amount: '$0.8 million',
    prodName: 'Education supplies',
    amount2: '$1.1 m'
  }],
  text: 'The September 2018 earthquake and tsunami off Sulawesi island affected more than 375,000 children in and around Pula city. UNICEF supported the Government-led response by using small aircraft to bring internationally procured supplies as close as possible to those in need. The Government then delivered them to the affected area. A strong in-country supply chain and Government pre-positioning quickly reduced the need for UNICEF support.',
  photo: 'i/products/indonesia.jpg',
  credit: 'Elementary school students in Talise village, Palu, Indonesia receive school supplies from UNICEF after their village was destroyed by the 28 September 2018 earthquake and tsunami. <br/> © UNICEF/UN0251788/Wilander'
}], ['sudan', {
  regName: 'South Sudan',
  data: [{
    icon: 'i/products/nutrition.svg',
    amount: '$5 million',
    prodName: 'Nutrition supplies',
    amount2: '$16.3 m'
  }, {
    icon: 'i/products/vaccines.svg',
    amount: '$4.5 million',
    prodName: 'Vaccines/biologicals',
    amount2: '$27.4 million'
  }, {
    icon: 'i/products/books.svg',
    amount: '$3.4 million',
    prodName: 'Education supplies',
    amount2: '$11.1 m'
  }],
  text: 'The world’s youngest country has been suffering from ongoing instability and conflict. In 2018, UNICEF procured 10.2 million doses of oral polio vaccine for South Sudan, along with other key supplies such as nutrition products and education kits. Pre-positioning of supplies in advance of the rainy season was crucial to their timely delivery in all regions of the country, with $12 million in savings achieved.',
  photo: 'i/products/sudan.jpg',
  credit: 'A child formerly associated with an armed group receives school supplies in Yambio, South Sudan. </br> © UNICEF/UN0158704/Prinsloo'
}], ['eks', {
  regName: 'Ethiopia, Kenya and Somalia',
  data: [{
    icon: 'i/products/nutrition.svg',
    amount: '$10.8 million',
    prodName: 'Nutrition supplies',
    amount2: '$26.9 m'
  }, {
    icon: 'i/products/med_devices.svg',
    amount: '$7.5 million',
    prodName: 'Medical devices',
    amount2: '$33.7 million'
  }, {
    icon: 'i/products/water.svg',
    amount: '$5.1 million',
    prodName: 'Water & sanitation',
    amount2: '$6.8 m'
  }],
  text: 'Seasonal climate-related floods and droughts exacerbated acute food insecurity, malnutrition and water shortages in Ethiopia, Kenya and Somalia. In response, UNICEF worked closely with partners to deliver nearly 2,740 metric tonnes of RUTF for the treatment of severe acute malnutrition, as well as essential medical and water and sanitation supplies. Lead times for the RUTF delivery were optimized by procuring 98 per cent of the product from suppliers in the region.',
  photo: 'i/products/eks.jpg',
  credit: 'A young daughter chews on her mother’s hijab while the two wait to see a health worker in Hargeisa, Somalia. </br> © UNICEF/UN0158445'
}]]);

document.addEventListener('DOMContentLoaded', function () {
	console.log(Modernizr.objectfit);
	if (Modernizr.objectfit === false) {
		//console.log('DOMContentLoaded');
		Array.prototype.forEach.call(document.querySelectorAll('img[data-object-fit]'), function (image) {
			(image.runtimeStyle || image.style).background = 'url("' + image.src + '") no-repeat 50%/' + (image.currentStyle ? image.currentStyle['object-fit'] : image.getAttribute('data-object-fit'));
			image.src = 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'' + image.width + '\' height=\'' + image.height + '\'%3E%3C/svg%3E';
		});
	}
});

$(document).ready(function ($) {
	console.log('rtl2');
	$body = $('body');
	$logo = $('.logo');
	$menuTrigger = $('.menuTrigger');
	$menuTriggerHamburger =  $('.menu_trigger');
	$menuTriggerTitle = $('.menu_trigger_text')
	$menu = $('.main_menu');
	$chaptersBlock = $('.chaptersJsBlock');
	$chaptersImg = $('.chaptersJsImg');
	$header = $('.header');
	$popupJsBlock = $('.popupJsBlock');
	$chaptersSlider = $('.chaptersSlider');
	$chaptersImgSlider = $('.chaptersImgSlider');
	$graphTabsLink = $('.graphTabsLink');
	$graphTabsBlock = $('.graphTabsBlock');

	if ($('#table1').length) {
		$('#table1').DataTable( {
		  "columns": [
		    null,
		    { "type": "num-fmt" },
		  ]
		});
	}

	if ($('#table2').length) {
		$('#table2').DataTable({
			"order": [[ 5, "asc" ]],
		  "columns": [
		    null,
				null,
		    { "type": "num-fmt" },
				{ "type": "num-fmt" },
				{ "type": "num-fmt" },
				null,
		  ]
		});
	}

	if ($('#table3').length) {
		$('#table3').DataTable( {
		  "columns": [
		    null,
		    { "type": "num-fmt" },
		  ]
		});
	}

	if ($('#table4').length) {
		$('#table4').DataTable( {
		  "columns": [
		    null,
		    { "type": "num-fmt" },
		  ]
		});
	}

	if ($('#table5').length) {
		$('#table5').DataTable({
		  "columns": [
		    null,
		    { "type": "num" },
				{ "type": "num" },
		  ]
		});
	}

	$chaptersBlock
		.on( "mouseenter", function() {
			var $this = $(this),
					thisData = $this.data('img'),
					$targetEl = $('.chaptersJsImg[data-img=' + thisData + ']');

			$chaptersBlock.addClass('disabled_state');
			$this.addClass('active_state');
			$targetEl.addClass('active_state');
		})
		.on( "mouseleave", function() {
			$chaptersImg.removeClass('active_state');
			$chaptersBlock.removeClass('disabled_state active_state');
		});

	$popupJsBlock
		.on("mouseenter", function() {
			if (windowWidth >= mediaPoint1) {
				var $this = $(this),
						thisData = $this.data('img'),
						$targetEl = $('.chaptersJsImg[data-img=' + thisData + ']');

				$chaptersBlock.addClass('disabled_state');
				$targetEl.addClass('active_state');
			}
		})
		.on( "mouseleave", function() {
			if (windowWidth >= mediaPoint1) {
				$chaptersImg.removeClass('active_state');
				$chaptersBlock.removeClass('disabled_state active_state');
			}
		});

	$graphTabsLink.on('click', function(e) {
		e.preventDefault();

		var $this = $(this),
				currentData = $this.data('tab');

		$graphTabsLink.removeClass('active_state');
		$graphTabsBlock.removeClass('active_tab');

		$('.graphTabsBlock' + '[data-tab="' + currentData + '"]').addClass('active_tab');
		$this.addClass('active_state');
	});

	// Tabs
	new Tabs($('#designTabs'));

	new Tabs($('#annexesTabs'));

	// World Map
	new WorldMap($('#worldMap'));

	// Timeline Popup
	new TimeLinePopup($('#timelinePopup'), $('.timeline_decade_round'), decadesContent);

	var WorldMapPopup1 = new WorldMapPopup($('#worldMapPopup'), $('.world_map_region_item_mark'), regionsCollection);
  var WorldMapTabs = new Tabs($('#regionTabs'));
  WorldMapPopup1.onAfterClose(function () {
    WorldMapTabs.reset();
  });


	$menuTrigger.on('click', function () {
		if ($body.hasClass('menu_open')) {
			$body.removeClass('menu_open');
			$menu.removeClass('active_mod');
			$logo.removeClass('active_mod');
			$menuTriggerHamburger.removeClass('active_mod');
			$menuTriggerTitle.html("menu");
			$(this).removeClass('active_mod');
		} else {
			$body.addClass('menu_open');
			$logo.addClass('active_mod');
			$menuTriggerHamburger.addClass('active_mod');
			$menu.addClass('active_mod');
			$menuTriggerTitle.html("close")
			$(this).addClass('active_mod');
		}
	});

	// members popup

	$('.members_item_in').on('click', function(e) {
		e.preventDefault();
		$('.members_popup').addClass('active_mod');
		indexPopup = $(this).data('popup');
		contentPopup = memberPopup[indexPopup];
		$('.members_name.popup_mod').html(contentPopup.name);
		$('.members_job.popup_mod').html(contentPopup.job);
		$('.members_country.popup_mod').html(contentPopup.country);
		$('.members_text p').html(contentPopup.text);
		if (contentPopup.img) {
			$('.members_popup_col.img_mod').html('').append('<div class="members_popup_img_wrap"><img class="members_popup_img" src="' + contentPopup.img + '" alt="member image" /><span class="photo_credit_banner2">'+contentPopup.credit+'</span></div>')

		}
		if (contentPopup.img_list) {
			var images =  contentPopup.img_list.map(function(item,index) {
				return ('<div class="members_popup_img_item"><div class="members_popup_img_wrap"><img class="members_popup_img" src="' + item + '" alt="member image" /><span class="photo_credit_banner2">'+contentPopup.credit_list[index]+'</span></div></div>')
			})
			$('.members_popup_col.img_mod').html('').append('<div class="members_popup_img_list">' + images + '</div>')
		}
	})

	$('.members_popup_close').on('click', function(e) {
		e.preventDefault();
		$('.members_popup').removeClass('active_mod');
	})

	// popup

	$('.popupBtnLink').on('click', function(e) {
		e.preventDefault();
		$body.addClass('popup_mod');

		if ($(this).hasClass('chapters_info_btn')) {
			$('.popup_wrap.foreword_mod').addClass('active_mod');
			$('.section.chapters_mod').addClass('blur_mod');
		}

		if ($(this).hasClass('partnership_link')) {
			$('.popup_wrap.text_mod').addClass('active_mod');
			indexPopup = $(this).data('popup');
			contentPopup = partnershipPopup[indexPopup];
			$('.popup_content').html('').append('<h2></h2><ul></ul>')
			$('.popup_content h2').html(contentPopup.title);
			textPopup = contentPopup.text.map(function(item) {
				return '<li>' + item + '</li>';
			})
			$('.popup_content ul').html(textPopup);
		}

		if ($(this).hasClass('diagram_item_in')) {
			$('.popup_wrap.text_mod').addClass('active_mod');
			indexPopup = $(this).data('popup');
			contentPopup = collabPopup[indexPopup];

			textPopup = contentPopup.map(function(item) {
				return '<h3>' + item.title + '</h3><p>' + item.text + '</p>'
			})

			$('.popup_content').html('').append(textPopup);
		}


	});

	$('.popup_close, .overlay').on('click', function(e) {
		e.preventDefault();
		$body.removeClass('popup_mod');
		$('.section.chapters_mod').removeClass('blur_mod');

		$('.popup_wrap').removeClass('active_mod');

		$chaptersImg.removeClass('active_state');
		$chaptersBlock.removeClass('disabled_state active_state');
	});

	$('.dropdown').on('click',function(e){
		$(this).toggleClass('is-active');
	});


	$('.service_slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: true
	});
	svg4everybody();

});

$(window).on('load', function () {
	updateSizes();
	loadFunc();
});

$(window).on('resize', function () {
	resizeFunc();
});

// $(window).on('scroll', function () {
// 	console.log('scrollami');
// 	scrollFunc();
// });

$(window).on('scroll', $.throttle(16, scrollFunc));

function loadFunc() {
	calcViewportHeight();

	checkInitSlider();
}

function resizeFunc() {
	updateSizes();

	calcViewportHeight();

	checkInitSlider();
}

function checkInitSlider() {
	if (windowWidth < mediaPoint1) {
		if (!$chaptersSlider.hasClass('slick-slider')) {
			initSliderFunc();
		}
	} else {
		if ($chaptersSlider.hasClass('slick-initialized')) {
			$chaptersSlider.slick("unslick");
			$chaptersImgSlider.slick("unslick");
		}
	}
}


function initSliderFunc() {
	$chaptersSlider.slick({
		infinite: true,
		slidesToShow: 1,
		mobileFirst: true,
		slidesToScroll: 1,
		fade: true,
		dots: true,
		arrows: false,
		asNavFor: $chaptersImgSlider
	});

	$chaptersImgSlider.slick({
		infinite: true,
		slidesToShow: 1,
		mobileFirst: true,
		slidesToScroll: 1,
		asNavFor: $chaptersSlider,
		fade: true,
		dots: false,
		arrows: false
	});
}

function scrollFunc() {
	$scrollTop = $(window).scrollTop();
	headerScroll();
}

function headerScroll() {
	if($scrollTop > 10 && !$header.hasClass('scroll_mod')) {
		$header.addClass('scroll_mod');
	} else if ($scrollTop < 10) {
		$header.removeClass('scroll_mod');
	}
}

function calcViewportHeight() {
	if (isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch) {
		var vh = window.innerHeight * 0.01;
		// var vh2 = document.documentElement.clientHeight * 0.01;

		document.documentElement.style.setProperty('--vh', vh + 'px');
	}
}

function updateSizes() {
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;
}

// if ('objectFit' in document.documentElement.style === false) {
// 	document.addEventListener('DOMContentLoaded', function () {
// 		Array.prototype.forEach.call(document.querySelectorAll('img[data-object-fit]'), function (image) {
// 			(image.runtimeStyle || image.style).background = 'url("' + image.src + '") no-repeat 50%/' + (image.currentStyle ? image.currentStyle['object-fit'] : image.getAttribute('data-object-fit'));
//
// 			image.src = 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'' + image.width + '\' height=\'' + image.height + '\'%3E%3C/svg%3E';
// 		});
// 	});
// }

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandom(min, max) {
	return Math.random() * (max - min) + min;
}

// const styles = ['color: #fff', 'background: #cf8e1f'].join(';');
//
// const message = 'Developed by Glivera-team https://glivera-team.com/';
//
// console.info('%c%s', styles, message);
